package model

type CommonResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
}

type MatchStatus struct {
	MatchId string `json:"matchId,omitempty"`
	Date string `json:"date,omitempty"`
	Team1 string `json:"team1,omitempty"`
	Team2 string `json:"team2,omitempty"`
	IsOpen string `json:"isOpen,omitempty"`
}


type ResponseWithSpecificKey struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	MatchInfo []MatchStatus `json:"matchInfo"`
}

type UserRank struct {
	Rank int `json:"rank,omitempty"`
	Picture string `json:"picture,omitempty"`
	Name string `json:"name,omitempty"`
	Points int `json:"points"`
}

type LeaderboardResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	TopPoints []UserRank `json:"topPoints"`
}

type MatchWinnerResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Match string `json:"match"`
	TopPlayers []MatchWiseGainer `json:"topPlayers,omitempty"`
}

type MatchesResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Matches []MatchSchedule`json:"matches,omitempty"`
}

type HistoryResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	History []UserHistory `json:"history,omitempty"`
}

type UndeclaredResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	MatchInfo []Match `json:"matchInfo"`
}

type StatisticsOutputJson struct {
    Team string `json:"team,omitempty"` 
    Supporters float32 `json:"supporters_ratio,omitempty"`
    BidAmount float32 `json:"bidAmount_ratio,omitempty"`
}

type StatisticsResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Statistics []StatisticsOutputJson`json:"statistics,omitempty"`
}

type LifeLineResponse struct {
	Code int `json:"code,omitempty"`
	Message string `json:"message,omitempty"`
	Data LifelineInfo `json:"data,omitempty"`
}