package model

type UpdateResultInputJson struct {
	TeamId string `json:"teamId,omitempty"`
	MatchId string `json:"matchId",omitempty"`
}

type BiddingStatusRequestJson struct {
	MatchId string `json:"matchId,omitempty"`
	OpenStatus bool `json:"openStatus,omitempty"`
	Team1Id string `json:"team1Id,omitempty"`
	Team2Id string `json:"team2Id,omitempty"`
}

type AdminLoginRequest struct {
	Email string `json:"email,omitempty"`
	Password string `json:"password,omitempty"`
}

type TestRequest struct {
	Name string `json:"name,omitempty"`
	Age int`json:"age,omitempty"`
}

type HistoryRequest struct {
	Email string `json:email,omitempty"`
}

type MatchWiseWinnerRequest struct {
	MatchId string `json:matchId,omitempty"`
}

type StatisticsInputJson struct {
	MatchId string `json:"matchId",omitempty"`
}

type RechargePointsInputJson struct {
	UserId string `json:"userId"`
	IdToken string `json:"idToken"`
}