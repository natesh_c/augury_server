package model

type Match struct {
	MatchId        string `json:"matchId,omitempty"`
	Team1          string `json:"team1,omitempty"`
	Team2          string `json:"team2,omitempty"`
	Team1Id        string `json:"team1_id,omitempty"`
	Team2Id        string `json:"team2_id,omitempty"`
	Team1BidUsers  int    `json:"team1BidUsers,omitempty"`
	Team2BidUsers  int    `json:"team2BidUsers,omitempty"`
	Team1BidAmount int    `json:"team1BidAmount,omitempty"`
	Team2BidAmount int    `json:"team2BidAmount,omitempty"`
	Date           string `json:"date,omitempty"`
}

type MatchSchedule struct {
	MatchNo                 int    `json:"matchNo,omitempty"`
	Team1Id                 string `json:"team1Id,omitempty"`
	Team2Id                 string `json:"team2Id,omitempty"`
	DateTime                string `json:"dateTime,omitempty"`
	Venue                   string `json:"venue,omitempty"`
	Participants            string `json:"participants,omitempty"`
	StartDateInMilliSeconds int64  `json:"startDateInMilliSeconds,omitempty"`
	IsOpen                  string `json:"matchStatus,omitempty"`
	TeamWonId               string `json:"teamWonId,omitempty"`
	IsResultKnown           bool   `json:"isResultKnown,omitempty"`
	MomPlayerName           string `json:"momPlayerName,omitempty"`
	MomPlayerUrl            string `json:"momPlayerUrl,omitempty"`
}
