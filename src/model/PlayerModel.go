package model

// Request

type PlayersRequest struct {
	TeamIds   []string `json:"teamIds,omitempty"`
}

// Response

type Player struct {
	PlayerId string `json:"playerId,omitempty"`
	TeamId string `json:"teamId,omitempty"`
	Name     string `json:"name,omitempty"`
	Role     string `json:"role,omitempty"`
	PicUrl     string `json:"picUrl,omitempty"`
}

type TeamSquad struct {
	Squad []Player `json:"squad,omitempty"`
}

type PlayerResponse struct {
	Code          int            `json:"code,omitempty"`
	Message       string         `json:"message,omitempty"`
	TeamSquads []TeamSquad 	`json:"teamSquads,omitempty"`
}