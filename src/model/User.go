package model

type User struct {
	UserId         string  `json:"userId,omitempty"`
	Percentage     float64 `json:"percentage,omitempty"`
	BidPoints      int     `json:"bidPoints,omitempty"`
	PreviousPoints int     `json:"previousPoints,omitempty"`
	AfterPoints    int     `json:"afterPoints,omitempty"`
}

type UserHistory struct {
	MatchId         int    `json:"matchId,omitempty"`
	BidPoints       int    `json:"bidPoints"`
	Team1Id         string `json:"team1Id,omitempty"`
	Team2Id         string `json:"team2Id,omitempty"`
	TeamWonId       string `json:"teamWonId,omitempty"`
	PlayerWonId     string `json:"playerWonId,omitempty"`
	PointsWon       int    `json:"pointsWon"`
	PointsAfter     int    `json:"pointsAfter"`
	ReducedPoints   int    `json:"reducedPoints"`
	PointsBefore    int    `json:"pointsBefore"`
	OptedTeam       string `json:"optedTeam"`
	OptedPlayerId   string `json:"optedPlayerId"`
	OptedPlayerName string `json:"optedPlayerName"`
	OptedPlayerUrl  string `json:"optedPlayerUrl"`
	IsResultKnown   bool   `json:"isResultKnown"`
}

type MatchWiseGainer struct {
	Image  string `json:"image"`
	Name   string `json:"name"`
	Points int    `json:"points"`
}

type LifelineInfo struct {
	Points         int  `json:"points"`
	RemainingLifes int  `json:"remainingLifes"`
	UIDisplay      bool `json:"uiDisplay"`
}
