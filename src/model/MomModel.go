package model

// Request

type MomResultRequest struct {
	MatchId  string `json:"matchId,omitempty"`
	PlayerId string `json:"playerId,omitempty"`
}

type MomPlayerRequest struct {
	IdToken     string `json:"idToken,omitempty"`
	UserEmail   string `json:"userEmail,omitempty"`
	MatchId     string `json:"matchId,omitempty"`
	OptedPlayer string `json:"optedPlayer,omitempty"`
}
