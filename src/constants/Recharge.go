package constants

const GetPointsAndLifelineOfUser = "SELECT leaderboard_points, lifeline FROM `User` WHERE user_id = (?)"

const GetOpenedMatchAndBiddedAmount = "SELECT COUNT(*) FROM user_history WHERE user_id = (?) " +
	"AND is_open = 1 AND bid_points != 0"

const UpdateLifelineToUsers = "UPDATE `User`" +
	"SET " +
	"points = points + 5000, " +
	"leaderboard_points = leaderboard_points + 5000, " +
	"lifeline = lifeline - 1 " +
	"WHERE " +
	"user_id = (?)"

const UpdatePointsBefore = "UPDATE user_history " +
	"SET points_before = (SELECT leaderboard_points FROM `User` WHERE `User`.user_id = (?))" +
	"WHERE is_open = 1 AND user_id = (?) "

const CheckSessionIdToken = "SELECT id_token FROM Session where user_id = (?) "
