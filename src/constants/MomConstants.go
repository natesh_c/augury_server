package constants

const ManOfMatchWonUsers = "SELECT user_id FROM user_history WHERE match_id = (?) AND opted_player = (?);"

const UpdateMomPoints = "UPDATE `User`" +
	"SET " +
	"points = points + 10000, " +
	"leaderboard_points = leaderboard_points + 10000 " +
	"WHERE " +
	"user_id = (?)"

const UpdateMomBidHistory = "UPDATE Bid_history SET man_of_match = (?) WHERE match_id =(?) AND is_result_known = 1"

const UpdateMomPlayer = "UPDATE `user_history` " +
	"SET opted_player = (?) " +
	"WHERE " +
	"match_id = (?) AND user_id = (?) AND is_open = 1"
