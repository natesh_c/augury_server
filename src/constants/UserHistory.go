package constants

const GetUserHistory = "SELECT " +
	"user_history.match_id, " +
	"user_history.bid_points, " +
	"Bid_history.team1_id, " +
	"Bid_history.team2_id, " +
	"Bid_history.team_won_id, " +
	"Bid_history.man_of_match, " +
	"Bid_history.is_result_known, " +
	"user_history.points_won, " +
	"user_history.points_after, " +
	"user_history.reduced_points, " +
	"user_history.opted_team, " +
	"user_history.points_before, " +
	"user_history.opted_player, " +
	"Player.name, " +
	"Player.url " +
	"FROM " +
	"user_history " +
	"INNER JOIN " +
	"Bid_history ON user_history.match_id = Bid_history.match_id " +
	"LEFT JOIN " +
	"Player ON Player.player_id = user_history.opted_player " +
	"WHERE " +
	"user_history.is_open = 0 " +
	"AND user_history.user_id = (?) "
