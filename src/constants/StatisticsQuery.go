package constants

const GetMatchStatistics = "SELECT " +
	"user_history.opted_team, " +
	"count(user_history.opted_team), " +
	"sum(user_history.bid_points) " +
"FROM " +
	"user_history " +
"WHERE " +
	"user_history.is_open = 1 " +
	"AND user_history.match_id = (?) " +
	"AND user_history.points_before >= 1000 AND opted_team != '' " +
"GROUP BY opted_team " 
