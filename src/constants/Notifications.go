package constants

//GetNotifications - To get all notifications for a particular user
const GetNotifications = "SELECT " +
	"Notification.notification_id, " +
	"Notification.type, " +
	"Notification.title, " +
	"Notification.message, " +
	"Notification.status, " +
	"Notification.points_won, " +
	"`Match`.team1_id, " +
	"`Match`.team2_id, " +
	"`Match`.match_id " +
	"FROM " +
	"Notification " +
	"INNER JOIN " +
	"`Match` ON Notification.match_id = `Match`.match_id " +
	"WHERE " +
	"Notification.user_id = (?) " +
	"AND is_shown = 0"

//GetTopper - To get the first topper in the leaderboard
const GetTopper = "SELECT " +
	"a.user_name, " +
	"a.leaderboard_points, " +
	"a.profile_url " +
	"FROM " +
	"`User` a " +
	"WHERE " +
	"a.leaderboard_points =" +
	"(" +
	"SELECT MAX(leaderboard_points)" +
	"FROM `User`" +
	")"


// Delete the Previous Notification related to topper
const DeleteTopper = "DELETE FROM Notification WHERE type = 'topper'"