package constants

const AllMatchesQuery = "SELECT DISTINCT `Match`.match_id, `Match`.team1_id, `Match`.team2_id, `Match`.start_date, `Match`.venue_id, `Match`.is_open, " +
	"Bid_history.team_won_id, Bid_history.is_result_known, " +
	"Player.name, Player.url " +
	"FROM `Match` " +
	"LEFT JOIN Bid_history " +
	"ON `Match`.match_id = Bid_history.match_id " +
	"LEFT JOIN Player ON Bid_history.man_of_match = Player.player_id"
