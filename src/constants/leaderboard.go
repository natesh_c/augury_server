package constants

const LeaderBoardNewQuery = "SELECT " +
	"`User`.user_name, " +
	"SUM(user_history.bid_points) + `User`.points AS current_holdings " +
	"FROM " +
	"`User` " +
	"INNER JOIN " +
	"user_history ON `User`.user_id = user_history.user_id " +
	"INNER JOIN " +
	"Bid_history ON user_history.match_id = Bid_history.match_id " +
	"WHERE " +
	"Bid_history.is_result_known = 0 " +
	"GROUP BY user_history.user_id " +
	"ORDER BY current_holdings DESC"

const LeaderBoardQuery = "SELECT user_name, leaderboard_points, profile_url FROM `User` ORDER BY leaderboard_points desc, user_name asc"

// const GetLeaderBoardTopper = "SELECT " +
// 	"leaderboard_points, profile_url, user_name" +
// "FROM " +
// 	"`User`` " +
// "ORDER BY " +
// 	"leaderboard_points DESC LIMIT 1 "

const UpdateTopperIntoNotification = "INSERT INTO Notification(user_id, type, title, message, points_won, match_id) " +
	"SELECT " +
	"user_id,'topper',(?),(?),(?),(?)" +
	"FROM " +
	"`User` "
