package constants

const UpdateUserHistoryNonParticipants = "UPDATE " +
	"user_history " +
	"SET reduced_points = 500, " +
	"points_after = points_before - 500 " +
	"WHERE match_id = (?) " +
	"AND bid_points = 0 " +
	"AND is_open = 1 " +
	"AND points_before >= 1000 "

const UpdateUserHistoryNonParticipantsBelowThousand = "UPDATE " +
	"user_history " +
	"SET points_after = points_before " +
	"WHERE match_id = (?) " +
	"AND bid_points = 0 " +
	"AND is_open = 1  " +
	"AND points_before < 1000 "

const UpdateNonParticipantsUserPoints = "UPDATE " +
	"`User` INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"AND user_history.reduced_points = 500 AND user_history.is_open = 1 " +
	"SET `User`.points = `User`.points - 500 " +
	"WHERE `User`.points >= 1000 "

const UpdateUserHistoryIsOpen = "UPDATE user_history " +
	"SET is_open = 0 WHERE user_history.match_id = (?)"

const UpdateMatchClosed = "UPDATE `Match` SET is_open = 'CLOSED' WHERE match_id = (?)"

const UpdateBidHistory = "UPDATE Bid_history " +
	"SET team1_bid_points = " +
	"(SELECT IFNULL(SUM(user_history.bid_points),DEFAULT(user_history.bid_points)) " +
	"FROM user_history WHERE user_history.opted_team = (?) " +
	"AND user_history.match_id = (?)), " +
	"Bid_history.team2_bid_points =  " +
	"(SELECT IFNULL(SUM(user_history.bid_points),DEFAULT(user_history.bid_points)) " +
	"FROM user_history WHERE user_history.opted_team =(?) " +
	"AND user_history.match_id = (?)), " +
	"Bid_history.team1_supporters = " +
	"(SELECT COUNT(user_history.user_id) FROM user_history " +
	"WHERE user_history.opted_team = (?) AND user_history.match_id = (?)), " +
	"Bid_history.team2_supporters =  " +
	"(SELECT COUNT(user_history.user_id) FROM user_history " +
	"WHERE user_history.opted_team = (?) AND  user_history.match_id = (?)), " +
	"Bid_history.non_participants = " +
	"((SELECT COUNT(user_id) FROM user_history where match_id = (?) AND points_before >= 1000) - " +
	"(Bid_history.team1_supporters + Bid_history.team2_supporters)) " +
	"WHERE Bid_history.match_id = (?)"

const UpdatePointsBeforeOnBidClose = "UPDATE user_history " +
	"INNER JOIN " +
	"( " +
	"SELECT " +
	"user_history.user_id, " +
	"(`User`.leaderboard_points) as points_before_u " +
	"FROM " +
	"`User` " +
	"INNER JOIN " +
	"user_history " +
	"ON User.user_id = user_history.user_id " +
	"WHERE user_history.is_open = 1 " +
	"GROUP BY user_history.user_id " +
	") as U " +
	"ON user_history.user_id = U.user_id " +
	"SET " +
	"user_history.points_before =  U.points_before_u " +
	"WHERE user_history.match_id = (?) "

const InsertMatchEntryOnBidHistory = "INSERT IGNORE INTO Bid_history (match_id, team1_id, team2_id) " +
	"SELECT match_id, team1_id, team2_id " +
	"FROM `Match` " +
	"WHERE match_id = (?)"
