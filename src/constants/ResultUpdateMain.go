package constants

const UpdateUserPointsOnResult = "UPDATE `User` " +
	"SET points = points + (?), " +
	"leaderboard_points = points " +
	"WHERE user_id = (?)"

const UpdateLeaderBoardPointsForLoser = "UPDATE `User` " +
	"INNER JOIN " +
	"user_history ON `User`.user_id = user_history.user_id " +
	"SET `User`.leaderboard_points = user_history.points_before - user_history.bid_points " +
	"WHERE user_history.match_id = (?) " +
	"AND user_history.opted_team != (?) " +
	"AND user_history.opted_team LIKE 't%' " +
	"AND user_history.bid_points > 0 "

const UpdateLeaderBoardPointsForWinner = "UPDATE `User` " +
	"INNER JOIN " +
	"user_history ON `User`.user_id = user_history.user_id " +
	"SET `User`.leaderboard_points = (user_history.points_before + user_history.points_won) - user_history.bid_points " +
	"WHERE user_history.match_id = (?) " +
	"AND user_history.opted_team = (?) " +
	"AND user_history.opted_team LIKE 't%' " +
	"AND user_history.bid_points > 0 "

const UpdateLeaderBoardForNonParticipants = "UPDATE " +
	"user_history " +
	"INNER JOIN " +
	"`User` ON user_history.user_id = `User`.user_id " +
	"SET `User`.leaderboard_points = user_history.points_before - 500 " +
	"WHERE " +
	"user_history.match_id = (?) AND user_history.bid_points = 0 AND user_history.points_before >= 1000 "

const UpdateUserHistoryPointsBeforeOnPreviousMatchResultUpdate = "UPDATE user_history " +
	"INNER JOIN " +
	"(SELECT user_id, points_after FROM user_history WHERE match_id = (?)) AS U " +
	"ON U.user_id = user_history.user_id " +
	"SET user_history.points_before = U.points_after " +
	"WHERE user_history.match_id > (?) "

const UpdatePointsBeforeBeforeResultUpdate = "UPDATE user_history INNER JOIN `User` " +
	"ON user_history.user_id = `User`.user_id " +
	"SET user_history.points_before = `User`.leaderboard_points " +
	"WHERE user_history.match_id = (?) AND `User`.points <= leaderboard_points AND leaderboard_points >= 1000"

const UpdatePointsAfterForWinners = "UPDATE user_history " +
	"SET user_history.points_after = points_before + points_won - bid_points " +
	"WHERE user_history.match_id = (?) and user_history.bid_points != 0 AND user_history.points_won != 0 "

const UpdateUserHistoryPointsAfterForNonParticipants = "UPDATE " +
	"user_history " +
	"SET points_after = points_before - 500 " +
	"WHERE match_id = (?) " +
	"AND bid_points = 0 " +
	"AND is_open = 0 " +
	"AND reduced_points = 500 " +
	"AND points_before >= 1000 "

const UpdatePointsAfterForNonParticipantsBelowThousands = "UPDATE " +
	"user_history " +
	"SET points_after = points_before, " +
	"reduced_points = 0 " +
	"WHERE match_id = (?) " +
	"AND bid_points = 0 " +
	"AND is_open = 0 " +
	"AND points_before < 1000 "

const UpdateNonParticipantsUserPointsOnResult = "UPDATE " +
	"`User` INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"AND user_history.reduced_points = 500 AND user_history.is_open = 0 AND points_before < 1000 " +
	"SET `User`.points = `User`.points + 500 " +
	"WHERE match_id = (?) "
