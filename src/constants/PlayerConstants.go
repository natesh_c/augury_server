package constants

// SQL Queries

const GetSquadQuery = "SELECT player_id, team_id, name, role, url " +
	"FROM Player " +
	"WHERE team_id = (?)"

const GetPlayerQuery = "SELECT player_id, team_id, name, role, url " +
	"FROM Player " +
	"WHERE player_id = (?)"

// Constants

const MSG_SUCCESS = "You have successfully retrieved players list"
const MSG_FAILURE = "Problem happened while fetching the players"
