package constants

const ParticipantsPointsWon = "SELECT points_won, user_id, bid_points FROM user_history " +
	"WHERE match_id = (?) AND bid_points != 0"

const InsertIntoNotification = "INSERT INTO Notification " +
	"(`user_id`,`match_id`,`type`,`status`,`is_shown`,`points_won`) " +
	"VALUES((?),(?),'result',(?),'0',(?))"

const SafeUpdatesOff = "SET SQL_SAFE_UPDATES = 0"
const SafeUpdatesOn = "SET SQL_SAFE_UPDATES = 1"

const UpdatePointsOnDraw = "UPDATE `User` " +
	"INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"SET `User`.points = `User`.points + user_history.bid_points " +
	"WHERE user_history.match_id = (?)"

const UpdateLeaderboardPointsOnDraw = "UPDATE `User` " +
	"INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"SET `User`.leaderboard_points = `User`.leaderboard_points - 500 " +
	"WHERE user_history.match_id = (?) AND user_history.bid_points = 0 AND `User`.leaderboard_points >= 1000"

const UpdateAfterPointsOnDraw = "UPDATE `User` " +
	"INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"SET user_history.points_after = `User`.points " +
	"WHERE user_history.match_id = (?)"

const UpdateTeamWonOnDraw = "UPDATE Bid_history " +
	"SET Bid_history.is_result_known = 1, " +
	"Bid_history.team_won_id = (?) " +
	"WHERE Bid_history.match_id = (?)"

const UpdateAfterPointsOnAbandoned = "UPDATE `User` " +
	"INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"SET user_history.points_after = user_history.points_before " +
	"WHERE user_history.match_id = (?)"

const UpdatePointsOnAbandonedNonParticipants = "UPDATE `User` " +
	"INNER JOIN user_history " +
	"ON `User`.user_id = user_history.user_id " +
	"SET `User`.points = `User`.points + 500 " +
	"WHERE user_history.match_id = (?) AND user_history.bid_points = 0 AND user_history.reduced_points = 500"
