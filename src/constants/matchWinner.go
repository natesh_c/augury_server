package constants

const MatchWinnerQuery = "SELECT user_name, profile_url, points_won, `Match`.team1_id, `Match`.team2_id, bid_points " +
	"FROM user_history, `Match`, `User`, Bid_history " +
	"WHERE " +
	"`User`.user_id=user_history.user_id AND " +
	"`Match`.match_id= user_history.match_id AND " +
	"`Match`.match_id= Bid_history.match_id  AND " +
	"user_history.match_id=(?) AND " +
	"points_won = (SELECT max(points_won) FROM user_history WHERE user_history.match_id=(?)) AND " +
	"Bid_history.team_won_id != 'DRAW' AND " +
	"Bid_history.team_won_id != 'ABANDONED'"

const GetLatestResultKnownMatchId = "SELECT match_id FROM Bid_history WHERE is_result_known = 1 ORDER BY match_id DESC LIMIT 1"
