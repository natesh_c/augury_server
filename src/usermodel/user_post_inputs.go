package usermodel

type LoginInput struct {
	UserId        string `json:"user_id,omitempty"`
	IdToken       string `json:"id_token,omitempty"`
	UserName      string `json:"user_name,omitempty"`
	ExpiresIn     int    `json:"expires_in,omitempty"`
	ExpiresAt     int    `json:"expires_at,omitempty"`
	FirstIssuedAt int    `json:"first_issued_at,omitempty"`
	AccessToken   string `json:"access_token,omitempty"`
	Date          string `json:"date,omitempty"`
	Points        int    `json:"points,omitempty"`
	ProfileUrl    string `json:"profile_url,omitempty"`
}

type UpdateBidPointInput struct {
	IdToken      string `json:"id_token,omitempty"`
	SessionID    string `json:"session_id,omitempty"`
	UserEmail    string `json:"user_email,omitempty"`
	MatchId      string `json:"match_id,omitempty"`
	SelectedTeam string `json:"selected_team,omitempty"`
	BiddedPoint  int    `json:"bidded_point,omitempty"`
	OptedPlayer  string `json:"opted_player,omitempty"`
}

type GetPointsInput struct {
	IdToken   string `json:"id_token,omitempty"`
	UserEmail string `json:"user_email,omitempty"`
}

type NotificationInput struct {
	IdToken        string `json:"id_token,omitempty"`
	UserEmail      string `json:"user_email,omitempty"`
	NotificationID int    `json:"notification_id",omitempty`
	MatchId        string `json:"match_id,omitempty"`
}
