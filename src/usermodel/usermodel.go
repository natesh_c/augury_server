package usermodel

import "github.com/ipl_server/src/model"

//TodaySchedule contains today match schedule data
type TodaySchedule struct {
	Id           int    `json:"id,omitempty"`
	MatchId      string `json:"match_id,omitempty"`
	Team_1       string `json:"team_1,omitempty"`
	Team_1_Id    string `json:"team_1_id,omitempty"`
	Team_2       string `json:"team_2,omitempty"`
	Team_2_Id    string `json:"team_2_id,omitempty"`
	Team_1_SN    string `json:"team_1_sn,omitempty"`
	Team_2_SN    string `json:"team_2_sn,omitempty"`
	StartingTime string `json:"starting_time,omitempty"`
	Venue        string `json:"venue,omitempty"`
}

//TodayMatches contains list of todaySchedule
type TodayMatches struct {
	Matches []TodaySchedule `json:"matches,omitempty"`
}

//Team contains information about each team
type Team struct {
	TeamId   string `json:"team_id,omitempty"`
	TeamName string `json:"team_name,omitempty"`
	TeamSN   string `json:"team_sn,omitempty"`
}

//Teams contains list of teams
type Teams struct {
	Teams []Team `json:"teams,omitempty"`
}

//Bid contains bid history of particular team
type Bid struct {
	MatchId          string       `json:"match_id,omitempty"`
	SelectedTeam     string       `json:"selected_team,omitempty"`
	BidPoints        int          `json:"bid_points"`
	UiLeft           string       `json:"ui_left"`
	UiRight          string       `json:"ui_right"`
	Is_Bid_Open      bool         `json:"is_bid_open"`
	OptedPlayer      model.Player `json:"opted_player"`
	EarnedPoints     int          `json:"earned_points"`
	EarnedPercentage int          `json:"earned_percentage"`
}

//Bids contains list of bids
type Bids struct {
	Bids   []Bid `json:"bid,omitempty"`
	Points int   `json:"points"`
}

//Notification : for showing notifications
type Notification struct {
	NotificationID   int    `json:"notification_id,omitempty"`
	NotificationType string `json:"notification_type,omitempty"`
	Status           string `json:"status,omitempty"`
	PointsWon        int    `json:"points_won"`
	Title            string `json:"title,omitempty"`
	Message          string `json:"message,omitempty"`
	MatchID          string `json:"match_id,omitempty"`
	Team1Id          string `json:"team1Id,omitempty"`
	Team2Id          string `json:"team2Id,omitempty"`
	Points           int    `json:"points"`
}

//Topper - contains the topper information in the leaderboard
type Topper struct {
	NotificationID   int    `json:"notification_id,omitempty"`
	NotificationType string `json:"notification_type,omitempty"`
	Name             string `json:"name,omitempty"`
	Points           int    `json:"points"`
	Picture          string `json:"profile_pic,omitempty"`
}

//Notifications : contains array of notification
type Notifications struct {
	Code          int            `json:"code,omitempty"`
	Message       string         `json:"message,omitempty"`
	Notifications []Notification `json:"notifications,omitempty"`
	Toppers       []Topper       `json:"toppers,omitempty"`
}
