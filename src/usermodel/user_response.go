package usermodel

type AllResults struct {
	MatchId        string `json:"match_id,omitempty"`
	Points         int    `json:"points"`
	Bidded_point   int    `json:"bid_point"`
	UserId         string `json:"user_id,omitempty"`
	IdToken        string `json:"id_token,omitempty"`
	SessionToken   string `json:"session_token,omitempty"`
	ProfileImage   string `json:"profile_image,omitempty"`
	UserPercentage int    `json:"user_percentage,omitempty"`
	UserGetPoints  int    `json:"user_get_points,omitempty"`
}

type Response struct {
	Status    string     `json:"status,omitempty"`
	Code      int        `json:"code,omitempty"`
	ErrorCode int        `json:"error_code,omitempty"`
	Message   string     `json:"message,omitempty"`
	Data      AllResults `json:"data,omitempty"`
}
