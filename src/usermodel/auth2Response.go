package usermodel

type GoogleResponseObj struct {
	Hd string `json:"hd,omitempty"`
	Email string `json:"email,omitempty"`
	EmailVerified string `json:"email_verified,omitempty"`
	Exp string `json:"exp,omitempty"`
	Iss string `json:"iss,omitempty"`
}