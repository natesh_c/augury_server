package logshandler

import (
	"log"
	"os"
)

//Info contains Result Info logger
var (
	Info *log.Logger
)

//InfoLoggerInit to open files for logger
func InfoLoggerInit(matchID string) (fp *os.File) {
	fileName := matchID + "_Info.log"
	path := "logs/" + matchID + "/"

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Println("Folder not exist! Creating Folder ", matchID)
		os.MkdirAll(path, 0777)
	}

	filePath := path + fileName
	fp, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalln("Failed to open log file", ":", err)
	}

	Info = log.New(fp, "Info: ", log.Ldate|log.Ltime|log.Lshortfile)
	return fp
}
