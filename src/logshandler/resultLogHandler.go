package logshandler

import (
	"log"
	"os"
)

//ResultInfo contains Result Info logger
var (
	ResultInfo *log.Logger
)

//ResultLoggerInit to open files for logger
func ResultLoggerInit(matchID string) (fp *os.File) {
	fileName := matchID + "_resultInfo.log"
	path := "logs/" + matchID + "/"

	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Println("Folder not exist! Creating Folder ", matchID)
		os.MkdirAll(path, 0777)
	}

	filePath := path + fileName
	fp, err := os.OpenFile(filePath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalln("Failed to open log file", ":", err)
	}

	ResultInfo = log.New(fp, "Result: ", log.Ldate|log.Ltime|log.Lshortfile)
	return fp
}
