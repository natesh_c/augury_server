package iplutilities

import (
	"database/sql"
)

func GetValue(name sql.NullString) string {
	if name.Valid {
		return name.String
	} else {
		return ""
	}
}

func GetBoolValue(value sql.NullBool) bool {
	if value.Valid {
		return value.Bool
	} else {
		return false
	}
}
