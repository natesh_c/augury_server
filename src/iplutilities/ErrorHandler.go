package iplutilities

// CheckError To check whether error is there!
func CheckError(err error) {
	if err != nil {
		panic(err)
	}
}
