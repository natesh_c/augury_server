package userhandler

import(
	"io/ioutil"
	"fmt"
	"net/http"
	"encoding/json"
	"github.com/ipl_server/src/usermodel"
)

func auth(googleUserObj usermodel.LoginInput) (authBool bool) {
	var token_id string = googleUserObj.IdToken
	const auth_url = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" 

	defer func() (authBool bool) {
		if r := recover(); r != nil{
			return false		
		}
		return true
	}()

	apiResponse, err := http.Get(auth_url + token_id)
	checkForPanic(err)
	
	defer apiResponse.Body.Close()
	body, err := ioutil.ReadAll(apiResponse.Body)
	checkForPanic(err)



	googleResponse := &usermodel.GoogleResponseObj{}
	err = json.Unmarshal([]byte(string(body)),googleResponse)
	checkForPanic(err)

	// Check if the user is from "tringapps" domain
	if googleResponse.Hd != "tringapps.com" {
		fmt.Println("The Email id is not from tringapps domain")
		return false
	}

	if googleResponse.EmailVerified != "true" {
		fmt.Println("Email id is not verified")
		return false
	}

	return true
}

func checkForPanic(err error){
	if err != nil{
		fmt.Println(err)
		panic(err)
	}
}