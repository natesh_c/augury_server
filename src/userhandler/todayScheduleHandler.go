package userhandler

import (
	"encoding/json"
	"net/http"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/usermodel"
	"github.com/ipl_server/src/iplutilities"
)

// TodayScheduleHandler : To get daily match schedules
func TodayScheduleHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	db := dbutilities.DbConnect()
	if req.Method == "GET" {
		query := "SELECT matchTable.match_id,matchTable.start_date,matchTable.team1_id ,matchTable.team2_id ,venueTable.venue FROM `Match` as matchTable INNER JOIN Venue as venueTable ON ( matchTable.is_open='OPENED' AND  matchTable.venue_id = venueTable.venue_id)"
		row, err := db.Query(query)
		iplutilities.CheckError(err)
		todaySchedule := usermodel.TodaySchedule{}
		todayMatches := usermodel.TodayMatches{}
		res := []usermodel.TodaySchedule{}
		for row.Next() {
			var matchID string
			var startDate string
			var team1ID string
			var team2ID string
			var venue string
			err := row.Scan(&matchID, &startDate, &team1ID, &team2ID, &venue)
			iplutilities.CheckError(err)

			todaySchedule.MatchId = matchID
			todaySchedule.Team_1_Id = team1ID
			todaySchedule.Team_2_Id = team2ID
			todaySchedule.StartingTime = startDate
			todaySchedule.Venue = venue
			res = append(res, todaySchedule)
			todayMatches.Matches = res
		}
		defer row.Close()
		json.NewEncoder(w).Encode(&todayMatches)
		defer db.Close()
	}
}