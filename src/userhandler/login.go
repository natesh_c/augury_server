package userhandler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/iplutilities"
	"github.com/ipl_server/src/usermodel"
)

//LogInHandler log in  handler
func LogInHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	var response usermodel.Response

	// Block Android users -
	// userAgent := r.Header.Get("User-Agent")
	// if userAgent == "okhttp/3.14.0" {

	// 	response.Code = 401
	// 	response.Message = "Unauthorized! We are not supporting for Android!"
	// 	w.WriteHeader(http.StatusUnauthorized)
	// 	json.NewEncoder(w).Encode(response)
	// 	return
	// }

	defer func() {
		if r := recover(); r != nil {
			response.Code = 500
			response.Message = "Problem happened while fetching the notifications"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	if r.Method == "POST" {

		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()

		data := usermodel.AllResults{}
		login := usermodel.LoginInput{Points: 10000}

		err := decoder.Decode(&login)
		checkForPanic(err)

		// Check whether the google token is valid
		var authBool bool = auth(login)
		if !authBool {
			response.Code = 500
			response.Message = "Authentication failed"
			response.Status = "error"
			response.ErrorCode = 501
			json.NewEncoder(w).Encode(&response)
			return
		}

		// Opening connection to Database
		db := dbutilities.DbConnect()
		defer db.Close()

		var count int
		_ = db.QueryRow("SELECT COUNT(*) FROM `User` WHERE `User`.user_id='" + login.UserId + "';").Scan(&count)
		if count == 0 {
			fmt.Println("New user")

			// Check how many matches he have missed and reduce the points
			var query string = "SELECT COUNT(match_id) FROM `Match` WHERE is_open = 'CLOSED'"
			err = db.QueryRow(query).Scan(&count)
			checkForPanic(err)

			var points int = 0

			if count < 20 {
				points = 10000 - (count * 500)
			}

			query = "INSERT INTO `User`(user_id,user_name,points,leaderboard_points,profile_url) values(?,?,?,?,?)"
			InsertUserStatement, err := db.Prepare(query)
			defer InsertUserStatement.Close()
			checkForPanic(err)

			_, err = InsertUserStatement.Exec(login.UserId, login.UserName, points, points, login.ProfileUrl)
			checkForPanic(err)

			// Updating the user_history for the new user if there is any current match under open
			rows, _ := db.Query("SELECT match_id FROM `Match` WHERE `Match`.is_open = 'OPENED'")
			defer rows.Close()

			for rows.Next() {
				var matchID string
				_ = rows.Scan(&matchID)

				// Inserting rows for each match
				stmt, _ := db.Prepare("INSERT INTO user_history (user_id, points_before, match_id, opted_team, bid_points, points_after, is_open, reduced_points) SELECT user_id, points, (?),'',0,0,1,0 FROM `User` WHERE user_id = (?)")
				defer stmt.Close()

				_, err = stmt.Exec(matchID, login.UserId)
				checkForPanic(err)
			}
		} else if count == 1 {
			updateUserPicQuery := "UPDATE `User` set profile_url=? where user_id=?;"
			updateUserPicStatement, err := db.Prepare(updateUserPicQuery)
			defer updateUserPicStatement.Close()
			checkForPanic(err)

			_, err = updateUserPicStatement.Exec(login.ProfileUrl, login.UserId)
			checkForPanic(err)
		}

		sessionToken, err := iplutilities.GenerateRandomString(32)
		checkForPanic(err)
		t := time.Now()
		logginTime := t.Format(time.UnixDate)

		var sessionCount int
		_ = db.QueryRow("SELECT count(*) FROM `Session` WHERE `Session`.user_id='" + login.UserId + "';").Scan(&sessionCount)
		if sessionCount == 0 {
			query1 := "INSERT INTO `Session`(user_id,id_token,date,access_token,expires_in,expires_at,first_issued_at,session_token) values(?,?,?,?,?,?,?,?)"
			userStatement, err := db.Prepare(query1)
			defer userStatement.Close()
			checkForPanic(err)

			_, err = userStatement.Exec(login.UserId, login.IdToken, logginTime, login.AccessToken, login.ExpiresIn, login.ExpiresAt, login.FirstIssuedAt, sessionToken)
			checkForPanic(err)
		} else if sessionCount == 1 {
			updateQuery := "UPDATE `Session` set user_id=?,id_token=?,date=?,access_token=?,expires_in=?,expires_at=?,first_issued_at=?,session_token=? where user_id=?;"
			userStatement, err := db.Prepare(updateQuery)
			defer userStatement.Close()
			checkForPanic(err)

			_, err = userStatement.Exec(login.UserId, login.IdToken, logginTime, login.AccessToken, login.ExpiresIn, login.ExpiresAt, login.FirstIssuedAt, sessionToken, login.UserId)
			checkForPanic(err)

		}
		data.UserId = login.UserId
		data.IdToken = login.IdToken
		data.SessionToken = sessionToken
		data.ProfileImage = login.ProfileUrl

		response.Code = 200
		response.Message = "User logged in successfull!"
		response.Status = "success"
		response.Data = data
		json.NewEncoder(w).Encode(&response)

	}

}
