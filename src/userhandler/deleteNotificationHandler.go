package userhandler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/usermodel"
	"github.com/ipl_server/src/iplutilities"
)

//DeleteNotificationsHandler : To delete notifications on home page about results
func DeleteNotificationsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	db := dbutilities.DbConnect()
	defer r.Body.Close()
	defer db.Close()
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var inputJSON usermodel.NotificationInput
		derr := decoder.Decode(&inputJSON)
		iplutilities.CheckError(derr)
		userEmail := inputJSON.UserEmail
		notificationID := inputJSON.NotificationID

		query := "DELETE FROM `Notification` WHERE `Notification`.user_id=? AND notification_id=?"
		statement, err := db.Prepare(query)
		defer statement.Close()
		iplutilities.CheckError(err)
		_, err = statement.Exec(userEmail, notificationID)
		iplutilities.CheckError(err)
		fmt.Println("Deleted Notification ID ", notificationID)
		response := usermodel.Response{}
		response.Code = 200
		response.Message = "Deleted notification successfully!"
		response.Status = "success"
		json.NewEncoder(w).Encode(&response)
	}
}
