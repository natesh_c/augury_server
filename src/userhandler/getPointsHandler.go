package userhandler

import (
	"encoding/json"
	"net/http"

	"github.com/ipl_server/src/handler"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/iplutilities"
	"github.com/ipl_server/src/usermodel"
)

//GetPointsHandler : To get the bid points of teams
func GetPointsHandler(w http.ResponseWriter, r *http.Request) {
	db := dbutilities.DbConnect()
	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var inputJSON usermodel.GetPointsInput
		err := decoder.Decode(&inputJSON)
		iplutilities.CheckError(err)
		userEmail := inputJSON.UserEmail
		query := "SELECT history.match_id, opted_team, history.bid_points, history.is_open, history.opted_player, `User`.points FROM user_history AS history INNER JOIN `Match` AS matchTable ON history.match_id = matchTable.match_id AND history.is_open=1 INNER JOIN `User` ON history.user_id = `User`.user_id WHERE history.user_id = '" + userEmail + "';"
		row, err := db.Query(query)
		iplutilities.CheckError(err)
		bid := usermodel.Bid{BidPoints: 0}
		bids := usermodel.Bids{Points: 0}
		res := []usermodel.Bid{}
		rowCount := 0
		for row.Next() {
			rowCount++
			var matchID, optedTeam, optedPlayer string
			var bidPoints, isOpen, points int
			err := row.Scan(&matchID, &optedTeam, &bidPoints, &isOpen, &optedPlayer, &points)
			iplutilities.CheckError(err)
			bid.MatchId = matchID
			bid.SelectedTeam = optedTeam
			bid.BidPoints = bidPoints
			bid.UiLeft = "tleft"
			bid.UiRight = "tright"
			bid.Is_Bid_Open = getBoolean(isOpen)
			bid.OptedPlayer = handler.GetPlayer(db, optedPlayer)
			bid.EarnedPoints, bid.EarnedPercentage = GetEarnedPoints(db, optedTeam, matchID, bidPoints)
			res = append(res, bid)
			bids.Bids = res
			bids.Points = points
		}
		defer row.Close()
		var userPoints int
		if rowCount == 0 {
			_ = db.QueryRow("Select points from `User` where user_id ='" + userEmail + "';").Scan(&userPoints)
			bids.Points = userPoints
		}
		w.Header().Set("Access-Control-Allow-Origin", "*")
		json.NewEncoder(w).Encode(&bids)
		defer r.Body.Close()
		defer db.Close()

	}
}

// ------------- INTERNAL FUNCTION ------------------------------

func getBoolean(number int) bool {
	if number == 1 {
		return true
	}
	return false
}
