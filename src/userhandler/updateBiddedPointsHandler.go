package userhandler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/handler"
	"github.com/ipl_server/src/logshandler"
	"github.com/ipl_server/src/usermodel"
)

// UpdateBidedPointsHandler : Updates the selected team and points
func UpdateBidedPointsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	db := dbutilities.DbConnect()
	defer r.Body.Close()
	defer db.Close()

	var response usermodel.Response

	// Transaction Begins
	tx, err := db.Begin()

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			checkForPanic(err)
			response.Code = 400
			response.Message = "Problem happened while fetching the notifications"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	checkForPanic(err)

	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var inputJSON usermodel.UpdateBidPointInput
		err := decoder.Decode(&inputJSON)
		checkForPanic(err)

		userEmail := inputJSON.UserEmail
		idToken := inputJSON.IdToken
		// sessionID := inputJSON.SessionID
		matchID := inputJSON.MatchId
		selectedTeam := inputJSON.SelectedTeam
		biddedPoint := inputJSON.BiddedPoint
		optedPlayer := inputJSON.OptedPlayer

		if matchID == "" || selectedTeam == "" {

			if optedPlayer != "" {

				handler.UpdateMomOptedPlayer(userEmail, idToken, matchID, optedPlayer, w)
				return
			} else {
				fmt.Println("Required Params empty")
				response.Code = 400
				response.Message = "Required Params are missing"
				w.WriteHeader(http.StatusBadRequest)
				json.NewEncoder(w).Encode(&response)
				return
			}
		}

		//Initialising bid logger
		f := logshandler.BidLoggerInit(matchID)
		Info := logshandler.BidInfo

		var id_token string
		_ = db.QueryRow("SELECT  id_token FROM Session WHERE user_id='" + userEmail + "';").Scan(&id_token)

		if idToken == id_token {
			//write condition to check bidded point greater than 0
			data := usermodel.AllResults{Points: 0}
			// tx, err := db.Begin()
			// CheckError(err)

			var isOpen int
			_ = db.QueryRow("SELECT count(is_open) FROM `user_history` WHERE `user_history`.user_id='" + inputJSON.UserEmail + "' AND `user_history`.is_open=1 AND `user_history`.match_id='" + matchID + "';").Scan(&isOpen)

			if isOpen == 1 {

				var prevMatchNotBidCount int
				_ = db.QueryRow("SELECT count(user_id) FROM user_history WHERE user_history.user_id='" + userEmail + "' AND user_history.is_open=1 AND bid_points = 0 AND user_history.match_id <'" + matchID + "';").Scan(&prevMatchNotBidCount)

				fmt.Print("prevMatchNotBidCount === %d", prevMatchNotBidCount)

				query1 := "SELECT points, bid_points FROM `User` INNER JOIN user_history ON `User`.user_id = user_history.user_id AND `User`.user_id='" + userEmail + "' AND user_history.match_id='" + matchID + "';"
				row, err := db.Query(query1)
				defer row.Close()
				checkForPanic(err)

				for row.Next() {
					var points int
					var biddedOldPoints int
					err := row.Scan(&points, &biddedOldPoints)
					checkForPanic(err)

					//Adding bidded old points and current points, then subtracting the newly bided point
					oldPoints := points + biddedOldPoints

					// If we have 2 match a day, check previous match bidded If not reduce 500 from current bid
					if prevMatchNotBidCount > 0 && (oldPoints-biddedPoint) < prevMatchNotBidCount*500 {

						fmt.Print("Your bid is invalid! Reduce 500 points for your previous match from the current bid!")
						response.Code = 400
						response.Message = "Your bid is invalid! Reduce 500 points for your previous match from the current bid!"
						response.ErrorCode = 501
						response.Status = "error"
						json.NewEncoder(w).Encode(&response)
						return

					} else {
						if biddedPoint <= oldPoints && biddedPoint >= 1000 && biddedPoint%100 == 0 {
							newPoints := oldPoints - biddedPoint
							data.Points = newPoints

							data.Bidded_point = biddedPoint
							data.MatchId = inputJSON.MatchId

							var bidQuery string

							if optedPlayer == "" {
								bidQuery = "UPDATE `user_history` SET opted_team=?, bid_points=? WHERE match_id=? AND user_id=? AND is_open = 1"
							} else {
								bidQuery = "UPDATE `user_history` SET opted_team=?, bid_points=?, opted_player=? WHERE match_id=? AND user_id=? AND is_open = 1"
							}
							statement, err := tx.Prepare(bidQuery)
							checkForPanic(err)
							defer statement.Close()

							if optedPlayer == "" {
								_, err = statement.Exec(selectedTeam, biddedPoint, matchID, userEmail)
							} else {
								_, err = statement.Exec(selectedTeam, biddedPoint, optedPlayer, matchID, userEmail)
							}
							checkForPanic(err)
						} else {
							//if biddedpoint is greater than oldpoints in table
							response.Code = 400
							response.Message = "Your bid point is invalid!"
							response.ErrorCode = 501
							response.Status = "error"
							json.NewEncoder(w).Encode(&response)
							return
						}
					}

				}
				query3 := "UPDATE `User` SET points=? WHERE user_id=? AND ((SELECT is_open FROM `Match` WHERE match_id = ?) = 'OPENED');"
				userStatement, err := tx.Prepare(query3)
				checkForPanic(err)
				defer userStatement.Close()

				_, err = userStatement.Exec(data.Points, inputJSON.UserEmail, inputJSON.MatchId)
				checkForPanic(err)

				tx.Commit()
				var updatedPoints int
				_ = db.QueryRow("SELECT points FROM `User` WHERE `User`.user_id='" + inputJSON.UserEmail + "';").Scan(&updatedPoints)
				data.Points = updatedPoints

				// Get user earned points
				userShare, userPercentage := GetEarnedPoints(db, selectedTeam, matchID, biddedPoint)
				data.UserPercentage = userPercentage
				data.UserGetPoints = userShare

				Info.Println(userEmail, "- Bid Point: ", biddedPoint, "| Selected Team: ", selectedTeam, "| Update: success")
				response.Code = 200
				response.Message = "Updated bid points successfully!"
				response.Status = "success"
				response.Data = data
				json.NewEncoder(w).Encode(&response)
				return

			} else {
				tx.Rollback()
				Info.Println(userEmail, "- Bid Point: ", biddedPoint, "| Selected Team: ", selectedTeam, "| Update: failure", "| Reason: Bid Closed!")
				response.Code = 200
				response.Message = "Bid has been Closed for this match!"
				response.ErrorCode = 502
				response.Status = "error"
				json.NewEncoder(w).Encode(&response)
				return
			}
		} else {
			tx.Rollback()
			Info.Println(userEmail, "- Bid Point: ", biddedPoint, "| Selected Team: ", selectedTeam, "| Update: warning", "| Reason: Trying to change Other Points!")
			response.Code = 400
			response.Message = "You don't have permission to change other points!"
			response.ErrorCode = 504
			response.Status = "error"
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(&response)
		}
		// else if idToken == id_token {
		// 	response.Code = 400
		// 	response.Message = "You have signed in with different browser, Kindly logout and login again!"
		// 	response.ErrorCode = 503
		// 	response.Status = "error"
		// 	w.WriteHeader(http.StatusBadRequest)
		// 	json.NewEncoder(w).Encode(&response)

		// }

		defer func() {
			if err := f.Close(); err != nil {
				panic(err)
			}
		}()

	}
}

func GetEarnedPoints(db *sql.DB, selectedTeam string, matchID string, biddedPoint int) (int, int) {

	var lostTeamTotalPoints, wonTeamTotalPoints, nonParticipantsCount int

	err := db.QueryRow("SELECT COALESCE(SUM(bid_points), 0) FROM `user_history` WHERE opted_team != '" + selectedTeam + "' AND match_id = '" + matchID + "'").Scan(&lostTeamTotalPoints)
	checkForPanic(err)

	err = db.QueryRow("SELECT COALESCE(SUM(bid_points), 0) FROM `user_history` WHERE opted_team = '" + selectedTeam + "' AND match_id = '" + matchID + "'").Scan(&wonTeamTotalPoints)
	checkForPanic(err)

	err = db.QueryRow("SELECT count(*) FROM `user_history` INNER JOIN `User` on user_history.user_id = `User`.user_id where bid_points = 0 AND `User`.leaderboard_points >= 1000 AND match_id = '" + matchID + "'").Scan(&nonParticipantsCount)
	checkForPanic(err)

	fmt.Println("lostTeamTotalPoints = ", lostTeamTotalPoints)
	fmt.Println("wonTeamTotalPoints = ", wonTeamTotalPoints)
	fmt.Println("nonParticipantsCount = ", nonParticipantsCount)

	userPercentage := 0
	totalPoints := lostTeamTotalPoints + (nonParticipantsCount * 500)

	if wonTeamTotalPoints > 0 {
		userPercentage = (biddedPoint * 100) / wonTeamTotalPoints
	}
	userShare := (userPercentage*totalPoints)/100

	return userShare, userPercentage
}
