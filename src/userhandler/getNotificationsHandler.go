package userhandler

import (
	"encoding/json"
	"net/http"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/usermodel"
)

//GetNotificationsHandler : To get notifications on home page about results
func GetNotificationsHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	db := dbutilities.DbConnect()
	defer r.Body.Close()
	defer db.Close()

	var response usermodel.Notifications

	defer func() {
		if r := recover(); r != nil {
			response.Code = 500
			response.Message = "Problem happened while fetching the notifications"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	count := 0

	if r.Method == "POST" {
		decoder := json.NewDecoder(r.Body)
		var inputJSON usermodel.GetPointsInput
		err := decoder.Decode(&inputJSON)
		checkForPanic(err)

		userEmail := inputJSON.UserEmail

		row, err := db.Query(constants.GetNotifications, userEmail)
		checkForPanic(err)
		defer row.Close()

		notification := usermodel.Notification{}
		res := []usermodel.Notification{}
		topper := usermodel.Topper{}
		res_topper := []usermodel.Topper{}

		Team_map := make(map[string]string)
		Team_map["t001"] = "AFG"
		Team_map["t002"] = "AUS"
		Team_map["t003"] = "BAN"
		Team_map["t004"] = "ENG"
		Team_map["t005"] = "IND"
		Team_map["t006"] = "NZ"
		Team_map["t007"] = "PAK"
		Team_map["t008"] = "SA"
		Team_map["t009"] = "SL"
		Team_map["t010"] = "WI"
		Team_map["TBC"] = "TBA"
	
	
		for row.Next() {
			count = count + 1
			var notificationID int
			var notificationType string
			var notificationTitle string
			var notificationMessage string
			var notificationStatus string
			var pointsWon int
			var team1_id string
			var team2_id string
			var matchID string

			err := row.Scan(&notificationID, &notificationType, &notificationTitle, &notificationMessage, &notificationStatus, &pointsWon, &team1_id, &team2_id, &matchID)

			checkForPanic(err)

			var points int
			_ = db.QueryRow("SELECT points FROM `User` WHERE `User`.user_id='" + userEmail + "';").Scan(&points)

			notification.NotificationID = notificationID
			notification.NotificationType = notificationType

			if notificationType == "topper" {
				topper.NotificationID = notificationID
				topper.NotificationType = notificationType
				topper.Name = notificationTitle
				topper.Points = pointsWon
				topper.Picture = notificationMessage
				res_topper = append(res_topper, topper)
			} else {
				notification.Title = notificationTitle
				notification.Message = notificationMessage
				notification.Status = notificationStatus
				notification.PointsWon = pointsWon
				notification.Team1Id = Team_map[team1_id]
				notification.Team2Id = Team_map[team2_id]
				notification.MatchID = matchID
				notification.Points = points
				res = append(res, notification)
			}
		}

		// trow, terr := db.Query(constants.GetTopper)
		// checkForPanic(terr)
		// defer trow.Close()

		// for trow.Next() {
		// 	count = count + 1
		// 	var name string
		// 	var profilePic string
		// 	var leaderboardPoints int

		// 	err := trow.Scan(&name, &leaderboardPoints, &profilePic)
		// 	checkForPanic(err)

		// 	topper.Name = name
		// 	topper.Points = leaderboardPoints
		// 	topper.Picture = profilePic
		// 	res_topper = append(res_topper, topper)
		// }

		if count > 0 {
			response.Code = 200
			response.Message = "Notifications Retrieved"
			response.Notifications = res
			response.Toppers = res_topper
			json.NewEncoder(w).Encode(&response)
		} else {
			response.Code = 103
			response.Message = "No Notifications"
			json.NewEncoder(w).Encode(&response)
		}

	}
}
