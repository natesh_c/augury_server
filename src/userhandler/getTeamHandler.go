package userhandler

import (
	"encoding/json"
	"net/http"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/usermodel"
	"github.com/ipl_server/src/iplutilities"
)

//GetTeamHandler : To get all the teams with ID and shortnames
func GetTeamHandler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	db := dbutilities.DbConnect()
	if req.Method == "GET" {
		query := "Select Team.team_id,Team.team_name,Team.team_shortName from `Team`;"
		row, err := db.Query(query)
		iplutilities.CheckError(err)
		team := usermodel.Team{}
		teams := usermodel.Teams{}
		res := []usermodel.Team{}
		for row.Next() {
			var teamID string
			var teamName string
			var teamShortName string
			err := row.Scan(&teamID, &teamName, &teamShortName)
			iplutilities.CheckError(err)
			team.TeamId = teamID
			team.TeamName = teamName
			team.TeamSN = teamShortName
			res = append(res, team)
			teams.Teams = res
		}
		defer row.Close()
		json.NewEncoder(w).Encode(&teams)
		defer db.Close()

	}
}
