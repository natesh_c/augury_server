package routes

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/ipl_server/src/handler"
	"github.com/ipl_server/src/userhandler"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"MatchWinners",
		"POST",
		"/matches/statistics",
		handler.GetStatistics,
	},
	Route{
		"MatchWinners",
		"GET",
		"/matches/results/winner",
		handler.GetMatchWinners,
	},
	Route{
		"UndeclaredMatches",
		"GET",
		"/matches/results/undeclared",
		handler.GetUndeclaredMatches,
	},
	Route{
		"ResultUpdate",
		"POST",
		"/matches/updateResult",
		handler.UpdateResult,
	},
	Route{
		"BiddingStatus",
		"POST",
		"/matches/biddingStatus",
		handler.BiddingStatus,
	}, Route{
		"BiddingStatusAll",
		"GET",
		"/matches/biddingStatus/all",
		handler.BiddingStatusAll,
	}, Route{
		"AdminAuth",
		"POST",
		"/admin/auth",
		handler.AdminAuth,
	}, Route{
		"UserHistory",
		"POST",
		"/user/history",
		handler.UserHistory,
	}, Route{
		"Recharge",
		"POST",
		"/users/points/recharge",
		handler.Recharge,
	}, Route{
		"LifelineInfo",
		"POST",
		"/users/lifeline",
		handler.LifelineInfo,
	}, Route{
		"AndroidDownload",
		"GET",
		"/mobile/download/android",
		handler.AndroidDownload,
	},
	Route{
		"Test",
		"GET",
		"/test",
		handler.Test,
	},
	Route{
		"Login",
		"POST",
		"/login",
		userhandler.LogInHandler,
	},
	Route{
		"GetPointsHandler",
		"POST",
		"/get_points",
		userhandler.GetPointsHandler,
	},
	Route{
		"GetTodaySchedule",
		"GET",
		"/get_today_schedule",
		userhandler.TodayScheduleHandler,
	},
	Route{
		"GetTeamHandler",
		"GET",
		"/get_teams",
		userhandler.GetTeamHandler,
	},
	Route{
		"UpdateBidedPointsHandler",
		"POST",
		"/update_bidded_points",
		userhandler.UpdateBidedPointsHandler,
	},
	Route{
		"LeaderBoardAPI",
		"GET",
		"/users/leaderboard/",
		handler.LeaderBoard,
	},
	Route{
		"MatchesAPI",
		"GET",
		"/matches/",
		handler.GetCurrentAndFutureMatches,
	},
	Route{
		"GetNotificationsHandler",
		"POST",
		"/get_notifications",
		userhandler.GetNotificationsHandler,
	},
	Route{
		"DeleteNotificationsHandler",
		"POST",
		"/delete_notification",
		userhandler.DeleteNotificationsHandler,
	},
	Route{
		"Players",
		"POST",
		"/players",
		handler.GetPlayers,
	},
	Route{
		"UpdateMomResult",
		"POST",
		"/matches/updateMomResult",
		handler.UpdateMomResult,
	},
	Route{
		"UpdateMomPlayer",
		"POST",
		"/matches/updateMomPlayer",
		handler.UpdateMomPlayer,
	},
}
