package dbutilities

import (
	"database/sql"
	_ "log"

	_ "github.com/go-sql-driver/mysql" // mysql
)

func init() {

}

//DbConnect  To connect database
func DbConnect() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := "TringPass"
	dbName := "IPL_Bidding"
	dbProtocol := "tcp"
	dbHost := "localhost"
	dbPort := "3306"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@"+dbProtocol+"("+dbHost+":"+dbPort+")/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	// log.Println("DB Connected Successfully!")
	return db
}
