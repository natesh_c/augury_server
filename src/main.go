package main

import (
	"log"
	"net/http"

	"github.com/ipl_server/src/routes"
	"github.com/rs/cors"
)

// our main function
func main() {
	log.Println("Staging Server running at Port 7000")
	router := routes.NewRouter()
	handler := cors.Default().Handler(router)
	log.Fatal(http.ListenAndServe(":7000", handler))
}
