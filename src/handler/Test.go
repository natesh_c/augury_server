package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func Test(w http.ResponseWriter, r *http.Request) {
	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	// Decoding the request body and putting it struct
	decoder := json.NewDecoder(r.Body)

	var input_json model.UpdateResultInputJson
	err := decoder.Decode(&input_json)

	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	fmt.Println("Input json from Match Result Updating Section: ")
	fmt.Println(input_json)

	// Connection to database
	db := dbutilities.DbConnect()

	// Transaction Begins
	tx, err := db.Begin()
	if err != nil {
		fmt.Println("-------------------------")
		fmt.Println(err)

		tx.Rollback()
		var response model.CommonResponse
		response.Code = 500
		response.Message = "Problem happened while starting the database transaction"
		json.NewEncoder(w).Encode(response)
		return
	}



	// Checking the number of participants in team1 and team2
	rows, err := db.Query("SELECT Bid_history.team1_id, Bid_history.team2_id, Bid_history.team1_supporters, Bid_history.team2_supporters, Bid_history.non_participants " +
		"FROM Bid_history WHERE Bid_history.match_id = 'm001'")

	for rows.Next(){
		var team1_supporters int 
		var team2_supporters int
		var team1_id string
		var team2_id string 
		var non_participants int

		err = rows.Scan(&team1_id, &team2_id, &team1_supporters, &team2_supporters, &non_participants)
		fmt.Println("Team1 supporters ", team1_supporters)
		fmt.Println("Team2 supporters ", team2_supporters)
		fmt.Println("Non participants ", non_participants)

		if team1_supporters == 0 && team2_supporters == 0 {
			fmt.Println("There are no team supporters for both of the team")

			// stmt, err := tx.Prepare("UPDATE Bid_history SET team_won_id = (?) , is_result_known = 1 WHERE match_id =(?)")
			// _, err = stmt.Exec(input_json.TeamId, input_json.MatchId)
			// if err != nil {
			// 	fmt.Println("-------------------------")
			// 	fmt.Println(err)
				
			// 	tx.Rollback()
			// 	var response model.CommonResponse
			// 	response.Code = 500
			// 	response.Message = "Problem happened creating the updating the table for match winner in Bid_history"
			// 	json.NewEncoder(w).Encode(response)
			// 	return
			// }
		} else if team1_supporters == 0  && input_json.TeamId == team1_id  {
			fmt.Println(input_json.TeamId + " has zero supporters and its the winning team")
			// stmt, err := tx.Prepare("UPDATE Bid_history SET team_won_id = (?) , is_result_known = 1 WHERE match_id =(?)")
			// _, err = stmt.Exec(input_json.TeamId, input_json.MatchId)
			// if err != nil {
			// 	fmt.Println("-------------------------")
			// 	fmt.Println(err)
				
			// 	tx.Rollback()
			// 	var response model.CommonResponse
			// 	response.Code = 500
			// 	response.Message = "Problem happened creating the updating the table for match winner in Bid_history"
			// 	json.NewEncoder(w).Encode(response)
			// 	return
			// }
		} else if team2_supporters == 0 && input_json.TeamId == team2_id {
			fmt.Println(input_json.TeamId + " has zero supporters and its the winning team")
			// stmt, err := tx.Prepare("UPDATE Bid_history SET team_won_id = (?) , is_result_known = 1 WHERE match_id =(?)")
			// _, err = stmt.Exec(input_json.TeamId, input_json.MatchId)
			// if err != nil {
			// 	fmt.Println("-------------------------")
			// 	fmt.Println(err)
				
			// 	tx.Rollback()
			// 	var response model.CommonResponse
			// 	response.Code = 500
			// 	response.Message = "Problem happened creating the updating the table for match winner in Bid_history"
			// 	json.NewEncoder(w).Encode(response)
			// 	return
			// }
		}



	}


	tx.Commit()
	var response model.CommonResponse
	response.Code = 200
	response.Message = "Test API success"
	json.NewEncoder(w).Encode(response)
	defer db.Close()
}