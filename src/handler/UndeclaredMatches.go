package handler

import (
	"encoding/json"
	_ "fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func GetUndeclaredMatches(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.UndeclaredResponse

	// Recover function
	defer func() {
		if r := recover(); r != nil {
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	db := dbutilities.DbConnect()
	defer db.Close()

	rows, err := db.Query("SELECT history.match_id, history.team1_id, history.team2_id, iplMatch.start_date, history.team1_supporters, history.team1_bid_points, history.team2_supporters, history.team2_bid_points FROM Bid_history AS history INNER JOIN `Match` AS iplMatch ON history.match_id = iplMatch.match_id WHERE iplMatch.is_open = 'CLOSED' AND history.is_result_known = 0")
	defer rows.Close()
	checkForPanic(err)

	var Matches []model.Match

	Team_map := make(map[string]string)
	Team_map["t001"] = "AFG"
	Team_map["t002"] = "AUS"
	Team_map["t003"] = "BAN"
	Team_map["t004"] = "ENG"
	Team_map["t005"] = "IND"
	Team_map["t006"] = "NZ"
	Team_map["t007"] = "PAK"
	Team_map["t008"] = "SA"
	Team_map["t009"] = "SL"
	Team_map["t010"] = "WI"

	for rows.Next() {
		var match_id string
		var team1_id string
		var team2_id string
		var start_date string
		var team1_bid_users int
		var team1_bid_points int
		var team2_bid_users int
		var team2_bid_points int

		err = rows.Scan(&match_id, &team1_id, &team2_id, &start_date, &team1_bid_users, &team1_bid_points, &team2_bid_users, &team2_bid_points)
		Matches = append(Matches, model.Match{Team1: Team_map[team1_id], Team2: Team_map[team2_id], Team1Id: team1_id, Team2Id: team2_id, Team1BidUsers: team1_bid_users, Team2BidUsers: team2_bid_users, Team1BidAmount: team1_bid_points, Team2BidAmount: team2_bid_points, Date: start_date, MatchId: match_id})

		checkForPanic(err)
	}

	err = rows.Err()
	checkForPanic(err)

	response.Code = 200
	response.Message = "Undeclared Matches Retreived"
	response.MatchInfo = Matches
	json.NewEncoder(w).Encode(response)
}
