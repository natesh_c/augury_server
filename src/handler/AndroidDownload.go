package handler

import (
	"net/http"
	"io"
	"os"
	"strconv"
)

func AndroidDownload(w http.ResponseWriter, r *http.Request) {
	
	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	filePath := "./mobile-build/android/augury.apk"

    Openfile, err := os.Open(filePath)
	defer Openfile.Close() //Close after function return
	checkForPanic(err)

	FileHeader := make([]byte, 512)
	//Copy the headers into the FileHeader buffer
	Openfile.Read(FileHeader)
	//Get content type of file
	// FileContentType := http.DetectContentType(FileHeader)
	FileStat, _ := Openfile.Stat()                     //Get info from file
	FileSize := strconv.FormatInt(FileStat.Size(), 10) //Get file size as a string

    w.Header().Set("Content-Disposition", "attachment; filename=augury.apk")
	w.Header().Set("Content-Type", "application/vnd.android.package-archive")
	w.Header().Set("Content-Length", FileSize)

	Openfile.Seek(0, 0)
	io.Copy(w, Openfile) //'Copy' the file to the client
	return
}