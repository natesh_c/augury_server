package handler

import (
	"encoding/json"
	_"fmt"
	"net/http"

	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func BiddingStatusAll(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.ResponseWithSpecificKey

	// Recover function
	defer func() {
		if r := recover(); r != nil{
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return		
		}
	}()


	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()


	Team_map := make(map[string]string)
	Team_map["t001"] = "AFG"
	Team_map["t002"] = "AUS"
	Team_map["t003"] = "BAN"
	Team_map["t004"] = "ENG"
	Team_map["t005"] = "IND"
	Team_map["t006"] = "NZ"
	Team_map["t007"] = "PAK"
	Team_map["t008"] = "SA"
	Team_map["t009"] = "SL"
	Team_map["t010"] = "WI"
	Team_map["TBC"] = "TBA"

	var MatchesStatus []model.MatchStatus

	rows, _ := db.Query("SELECT match_id, start_date, team1_id, team2_id, is_open FROM `Match`")
	defer rows.Close()

	for rows.Next() {
		var match_id string
		var date string
		var team1_id string
		var team2_id string
		var is_open string

		err := rows.Scan(&match_id, &date, &team1_id, &team2_id, &is_open)
		checkForPanic(err)

		MatchesStatus = append(MatchesStatus, model.MatchStatus{MatchId: match_id, Date: date, Team1: Team_map[team1_id], Team2: Team_map[team2_id], IsOpen: is_open})
	}
	
	err := rows.Err()
	checkForPanic(err)

	response.Code = 200
	response.Message = "All Matches Bidding Status fetched"
	response.MatchInfo = MatchesStatus
	json.NewEncoder(w).Encode(response)

}
