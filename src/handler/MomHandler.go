package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/iplutilities"
	"github.com/ipl_server/src/model"
)

func UpdateMomPlayer(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	fmt.Println("Updating Man of the Match player...")

	decoder := json.NewDecoder(r.Body)
	var inputJSON model.MomPlayerRequest
	err := decoder.Decode(&inputJSON)
	checkForPanic(err)
	defer r.Body.Close()

	var response model.CommonResponse

	defer func() {
		if r := recover(); r != nil {
			response.Code = 400
			response.Message = "Problem happened while updating Man of the Match player"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	userEmail := inputJSON.UserEmail
	idToken := inputJSON.IdToken
	matchID := inputJSON.MatchId
	optedPlayer := inputJSON.OptedPlayer

	UpdateMomOptedPlayer(userEmail, idToken, matchID, optedPlayer, w)

}

func UpdateMomOptedPlayer(userEmail string, idToken string, matchID string, optedPlayer string, w http.ResponseWriter) {

	db := dbutilities.DbConnect()
	defer db.Close()

	var response model.CommonResponse

	if matchID == "" || optedPlayer == "" || idToken == "" || userEmail == "" {
		fmt.Println("Required Params empty")
		response.Code = 400
		response.Message = "Required Params are missing"
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&response)
		return
	}

	var token string
	_ = db.QueryRow("SELECT id_token FROM Session WHERE user_id='" + userEmail + "';").Scan(&token)

	if idToken == token {

		statement, err := db.Prepare(constants.UpdateMomPlayer)
		checkForPanic(err)
		defer statement.Close()

		_, err = statement.Exec(optedPlayer, matchID, userEmail)
		checkForPanic(err)

		response.Code = 200
		fmt.Println("MOM player updated")
		response.Message = "Man of the match player updated successfully"
		json.NewEncoder(w).Encode(response)

	} else {
		response.Code = 400
		response.Message = "You don't have permission to change the player!"
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(&response)
	}
}

func UpdateMomResult(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	fmt.Println("Updating Man of the Match...")

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	// Transaction Begins
	tx, err := db.Begin()
	checkForPanic(err)

	var response model.CommonResponse

	decoder := json.NewDecoder(r.Body)
	var reqBody model.MomResultRequest
	decodeErr := decoder.Decode(&reqBody)
	checkForPanic(decodeErr)
	fmt.Println("MomResultRequest = ", reqBody)

	// Update Bid History table
	isMomUpdated := updateMomOnBidHistory(reqBody.MatchId, reqBody.PlayerId, tx)
	fmt.Println("isMomUpdated = ", isMomUpdated)

	if isMomUpdated == 0 {
		response.Code = 200
		fmt.Println("Match not yet finished")
		response.Message = "Match not yet finished"
		json.NewEncoder(w).Encode(response)
		return
	}

	rows, err := db.Query(constants.ManOfMatchWonUsers, reqBody.MatchId, reqBody.PlayerId)
	checkForPanic(err)

	defer rows.Close()

	for rows.Next() {

		var userID string

		err := rows.Scan(&userID)
		iplutilities.CheckError(err)

		// Update the User points and leaderboard points
		updateMomPoints(tx, userID)

		// Update the points before for the opened match
		updatePointsBeforeOnHistory(tx, userID)
	}

	// Recover function
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			response.Code = 500
			response.Message = "Error while updating man of the match points"
			json.NewEncoder(w).Encode(response)
			return
		} else {
			tx.Commit()
			response.Code = 200
			fmt.Println("MOM result updated")
			response.Message = "Man of the match updated successfully"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

}

func updateMomPoints(tx *sql.Tx, userID string) {

	stmt, err := tx.Prepare(constants.UpdateMomPoints)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(userID)
	checkForPanic(err)
}

func updatePointsBeforeOnHistory(tx *sql.Tx, userID string) {

	stmt, err := tx.Prepare(constants.UpdatePointsBefore)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(userID, userID)
	checkForPanic(err)
}

func updateMomOnBidHistory(matchID string, playerID string, tx *sql.Tx) int64 {

	stmt, err := tx.Prepare(constants.UpdateMomBidHistory)
	defer stmt.Close()
	checkForPanic(err)

	result, error := stmt.Exec(playerID, matchID)
	checkForPanic(error)

	count, _ := result.RowsAffected()

	return count
}
