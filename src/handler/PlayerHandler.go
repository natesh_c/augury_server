package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/iplutilities"
	"github.com/ipl_server/src/model"
)

func GetPlayers(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Access-Control-Allow-Origin", "*")
	fmt.Println("Fetching players...")

	db := dbutilities.DbConnect()
	defer r.Body.Close()
	defer db.Close()

	response := model.PlayerResponse{}

	defer func() {
		if r := recover(); r != nil {
			response.Code = 500
			response.Message = constants.MSG_FAILURE
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	if r.Method == "POST" {

		decoder := json.NewDecoder(r.Body)
		var reqBody model.PlayersRequest
		err := decoder.Decode(&reqBody)
		checkForPanic(err)
		fmt.Println("team ids...", reqBody)

		players := model.TeamSquad{}

		for _, teamId := range reqBody.TeamIds {

			players.Squad = getSquad(db, teamId)
			response.TeamSquads = append(response.TeamSquads, players)
		}
		response.Code = 200
		response.Message = constants.MSG_SUCCESS
		json.NewEncoder(w).Encode(&response)
	}
}

func getSquad(db *sql.DB, team_id string) []model.Player {

	rows, err := db.Query(constants.GetSquadQuery, team_id)
	checkForPanic(err)

	defer rows.Close()
	player := model.Player{}
	playerResponse := []model.Player{}

	for rows.Next() {
		var playerId string
		var teamId string
		var playerName string
		var playerRole string
		var playerUrl string

		err := rows.Scan(&playerId, &teamId, &playerName, &playerRole, &playerUrl)
		iplutilities.CheckError(err)

		player.PlayerId = playerId
		player.TeamId = teamId
		player.Name = playerName
		player.Role = playerRole
		player.PicUrl = playerUrl

		playerResponse = append(playerResponse, player)
	}
	return playerResponse
}

func GetPlayer(db *sql.DB, playerId string) model.Player {

	player := model.Player{}
	_ = db.QueryRow(constants.GetPlayerQuery, playerId).Scan(&player.PlayerId, &player.TeamId, &player.Name, &player.Role, &player.PicUrl)

	return player
}
