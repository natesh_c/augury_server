package handler

import (
	"database/sql"
	"encoding/json"
	_ "fmt"
	"net/http"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/logshandler"
	"github.com/ipl_server/src/model"
)

func UpdateResult(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.CommonResponse

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	// Transaction Begins
	tx, err := db.Begin()

	// Recover function
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	checkForPanic(err)

	// Decoding the request body and putting it struct
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var input_json model.UpdateResultInputJson
	err = decoder.Decode(&input_json)
	checkForPanic(err)

	//Initialising bid logger
	f := logshandler.ResultLoggerInit(input_json.MatchId)
	log := logshandler.ResultInfo

	defer func() {
		if err := f.Close(); err != nil {
			panic(err)
		}
	}()

	log.Println("Input Json from the Result Update", input_json)

	// When the match is Draw
	if input_json.TeamId == "DRAW" || input_json.TeamId == "ABANDONED" {
		Draw(w, input_json)
		return
	}

	// When the match is Abandoned without Toss
	if input_json.TeamId == "ABANDONED WITHOUT TOSS" {
		AbandonedWithoutToss(w, input_json)
		return
	}

	// Handling edge cases
	rows, err := db.Query("SELECT Bid_history.team1_id, Bid_history.team2_id, Bid_history.team1_supporters, Bid_history.team2_supporters, Bid_history.non_participants "+
		"FROM Bid_history WHERE Bid_history.match_id = (?)", input_json.MatchId)
	defer rows.Close()

	var isEdge bool = false
	for rows.Next() {
		var team1_supporters int
		var team2_supporters int
		var team1_id string
		var team2_id string
		var non_participants int

		err = rows.Scan(&team1_id, &team2_id, &team1_supporters, &team2_supporters, &non_participants)
		log.Println("Team1 supporters ", team1_supporters)
		log.Println("Team2 supporters ", team2_supporters)
		log.Println("Non participants ", non_participants)

		if team1_supporters == 0 && team2_supporters == 0 {
			log.Println("There are no team supporters for both of the team")
			updateBidHistory(input_json, tx)
			isEdge = true

		} else if team1_supporters == 0 && input_json.TeamId == team1_id {
			log.Println(input_json.TeamId + " has zero supporters and its the winning team")
			updateBidHistory(input_json, tx)
			isEdge = true

		} else if team2_supporters == 0 && input_json.TeamId == team2_id {
			log.Println(input_json.TeamId + " has zero supporters and its the winning team")
			updateBidHistory(input_json, tx)
			isEdge = true
		}
	}

	err = rows.Err()
	checkForPanic(err)

	if isEdge {
		tx.Commit()
		var response model.CommonResponse
		response.Code = 200
		response.Message = "Edge case Found! But result updated"
		json.NewEncoder(w).Encode(response)
		return
	}

	rows, err = tx.Query("SELECT user_history.bid_points, user_history.user_id, Bid_history.team1_id, Bid_history.team2_id, Bid_history.team1_bid_points, Bid_history.team2_bid_points, Bid_history.team1_supporters, Bid_history.team2_supporters, Bid_history.non_participants FROM user_history INNER JOIN Bid_history ON user_history.match_id = Bid_history.match_id AND Bid_history.match_id = ? AND user_history.opted_team = ?", input_json.MatchId, input_json.TeamId)
	defer rows.Close()
	checkForPanic(err)

	var bid_points int
	var user_id string
	var team1_id string
	var team2_id string
	var team1_bid_points int
	var team2_bid_points int
	var team1_supporters int
	var team2_supporters int
	var non_participants int

	var Users []model.User
	for rows.Next() {

		err = rows.Scan(&bid_points, &user_id, &team1_id, &team2_id, &team1_bid_points, &team2_bid_points, &team1_supporters, &team2_supporters, &non_participants)

		log.Println("Calculation for user id ", user_id)
		var percentage float64
		var total_points float64
		var user_point float64

		// Depending upon the winning team passing the argument for calc percentage
		if input_json.TeamId == team1_id {
			percentage = getPercentage(float64(bid_points), float64(team1_bid_points))
			log.Println("User share in percentage: %f", percentage)
			total_points = getTotalPoints(float64(team2_bid_points), float64(non_participants))
			log.Println("Total points to be shared %f", +total_points)
			user_point = getUserShare(percentage, total_points, float64(bid_points))
			log.Println("Updated user points %f ", user_point)
		} else {
			percentage = getPercentage(float64(bid_points), float64(team2_bid_points))
			log.Println("User share in percentage: %f", percentage)
			total_points = getTotalPoints(float64(team1_bid_points), float64(non_participants))
			log.Println("Total points to be shared %f", +total_points)
			user_point = getUserShare(percentage, total_points, float64(bid_points))
			log.Println("Updated user points %f ", user_point)
		}

		Users = append(Users, model.User{UserId: user_id, Percentage: percentage, BidPoints: bid_points, PreviousPoints: (int(user_point) - bid_points), AfterPoints: int(user_point)})
		log.Println("-----------------------------------------------------------")
	}

	err = rows.Err()
	checkForPanic(err)

	// Updating the points before for all users before updating the result
	stmt, err := tx.Prepare(constants.UpdatePointsBeforeBeforeResultUpdate)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId)
	checkForPanic(err)

	// Updating the total points of the user if he have won in the User table
	for _, user := range Users {
		stmt, err := tx.Prepare(constants.UpdateUserPointsOnResult)
		defer stmt.Close()
		checkForPanic(err)

		_, err = stmt.Exec(user.AfterPoints, user.UserId)
		checkForPanic(err)

		// Updating the points won in the user_history for the user
		stmt, err = tx.Prepare("UPDATE user_history SET user_history.points_won = (?) WHERE user_id = (?) AND match_id = (?)")
		defer stmt.Close()
		checkForPanic(err)

		_, err = stmt.Exec(user.AfterPoints, user.UserId, input_json.MatchId)
		checkForPanic(err)

		// Updating the after_points of the person who have won in the user_history table
		// stmt, err = tx.Prepare("UPDATE user_history SET points_after= (SELECT points FROM User where user_id = (?)) WHERE user_id = (?) AND match_id= (?)")
		// defer stmt.Close()
		// checkForPanic(err)

		// _, err = stmt.Exec(user.UserId, user.UserId, input_json.MatchId)
		// checkForPanic(err)
	}

	// Update the points_after for the winners
	stmt, err = tx.Prepare(constants.UpdatePointsAfterForWinners)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId)
	checkForPanic(err)

	// Updating the table for those who lost the match
	_, err = db.Query("SET SQL_SAFE_UPDATES = 0")
	checkForPanic(err)

	stmt, err = tx.Prepare("UPDATE user_history SET points_after = points_before - bid_points WHERE match_id = (?) AND opted_team != (?) AND opted_team LIKE 't%'")
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId, input_json.TeamId)
	checkForPanic(err)

	_, err = db.Query("SET SQL_SAFE_UPDATES = 1")
	checkForPanic(err)

	// Updating the Bidhistory
	updateBidHistory(input_json, tx)

	// Updating the points_before for the next matches from the current points_after
	stmt, err = tx.Prepare(constants.UpdateUserHistoryPointsBeforeOnPreviousMatchResultUpdate)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId, input_json.MatchId)
	checkForPanic(err)

	updateLeaderBoardPointsForWinner(tx, input_json)
	updateLeaderBoardPointsForLoser(tx, input_json)
	updateLeaderBoardForNonParticipants(tx, input_json)

	// updateNonParticipantsUserPointsOnResult(tx, input_json)
	updateUserHistoryPointsAfterForNonParticipants(tx, input_json)
	updatePointsAfterForNonParticipantsBelowThousands(tx, input_json)

	// Committing the first transaction
	tx.Commit()

	tx1, err := db.Begin()
	checkForPanic(err)

	// Delete the Previous Topper
	// _, err = db.Exec(constants.SafeUpdatesOff)
	// checkForPanic(err)

	_, err = db.Exec(constants.DeleteTopper)
	checkForPanic(err)

	_, err = db.Exec(constants.SafeUpdatesOn)
	checkForPanic(err)

	// Updating the notifcation Table
	rows, err = db.Query(constants.ParticipantsPointsWon, input_json.MatchId)
	defer rows.Close()
	checkForPanic(err)

	for rows.Next() {
		var points_won int
		var user_id string
		var bid_points int

		const lose string = "lose"
		const won string = "won"
		var result string = lose

		err := rows.Scan(&points_won, &user_id, &bid_points)
		checkForPanic(err)

		if points_won > 0 {
			result = won
			points_won = points_won - bid_points
		}

		stmt, err := tx1.Prepare(constants.InsertIntoNotification)
		defer stmt.Close()
		checkForPanic(err)

		_, err = stmt.Exec(user_id, input_json.MatchId, result, points_won)
		checkForPanic(err)
	}
	err = rows.Err()
	checkForPanic(err)

	// Updating the Topper in the Notification Table
	var topper_points int
	var topper_profile_url string
	var topper_name string

	rows, err = db.Query(constants.GetTopper)
	defer rows.Close()
	checkForPanic(err)

	for rows.Next() {
		err = rows.Scan(&topper_name, &topper_points, &topper_profile_url)
		checkForPanic(err)
		log.Println("Leaderboard topper")
		log.Println(topper_name)
		log.Println(topper_profile_url)
		log.Println(topper_points)

		stmt, err = db.Prepare(constants.UpdateTopperIntoNotification)
		defer stmt.Close()
		checkForPanic(err)

		_, err = stmt.Exec(topper_name, topper_profile_url, topper_points, input_json.MatchId)
		checkForPanic(err)

	}

	tx1.Commit()

	response.Code = 200
	response.Message = "Match Result Updated"
	json.NewEncoder(w).Encode(response)
}

func getUserShare(user_percentage float64, total_points float64, bid_points float64) float64 {
	return (((user_percentage * total_points) / 100) + bid_points)
}

func getTotalPoints(oppositeTeam float64, non_participants float64) float64 {
	return (oppositeTeam + (non_participants * 500))
}

func getPercentage(points float64, total_points float64) float64 {
	return ((points * 100) / total_points)
}

func updateBidHistory(json model.UpdateResultInputJson, tx *sql.Tx) {

	stmt, err := tx.Prepare("UPDATE Bid_history SET team_won_id = (?) , is_result_known = 1 WHERE match_id =(?)")
	_, err = stmt.Exec(json.TeamId, json.MatchId)
	defer stmt.Close()
	checkForPanic(err)
}

func updateLeaderBoardForNonParticipants(tx *sql.Tx, json model.UpdateResultInputJson) {

	stmt, err := tx.Prepare(constants.UpdateLeaderBoardForNonParticipants)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(json.MatchId)
	checkForPanic(err)
}

func updateLeaderBoardPointsForWinner(tx *sql.Tx, json model.UpdateResultInputJson) {

	stmt, err := tx.Prepare(constants.UpdateLeaderBoardPointsForWinner)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(json.MatchId, json.TeamId)
	checkForPanic(err)

}

func updateLeaderBoardPointsForLoser(tx *sql.Tx, json model.UpdateResultInputJson) {

	stmt, err := tx.Prepare(constants.UpdateLeaderBoardPointsForLoser)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(json.MatchId, json.TeamId)
	checkForPanic(err)
}

func updateUserHistoryPointsAfterForNonParticipants(tx *sql.Tx, json model.UpdateResultInputJson) {

	stmt, err := tx.Prepare(constants.UpdateUserHistoryPointsAfterForNonParticipants)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(json.MatchId)
	checkForPanic(err)
}

func updatePointsAfterForNonParticipantsBelowThousands(tx *sql.Tx, json model.UpdateResultInputJson) {

	stmt, err := tx.Prepare(constants.UpdatePointsAfterForNonParticipantsBelowThousands)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(json.MatchId)
	checkForPanic(err)
}

// func updateNonParticipantsUserPointsOnResult(tx *sql.Tx, json model.UpdateResultInputJson) {

// 	stmt, err := tx.Prepare(constants.UpdateNonParticipantsUserPointsOnResult)
// 	defer stmt.Close()
// 	checkForPanic(err)

// 	_, err = stmt.Exec(json.MatchId)
// 	checkForPanic(err)
// }
