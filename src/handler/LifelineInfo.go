package handler

import (
	 "encoding/json"
	"fmt"
	"net/http"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
	"github.com/ipl_server/src/constants"
)


func LifelineInfo (w http.ResponseWriter, r *http.Request) {
	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	
	var response model.LifeLineResponse
	var sessionMismatch bool = false
	var UIdisplayFlag = false

	// Get the User Details
	var user_points int
	var user_lifeline int

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()


	// Transaction Begins
	tx, err := db.Begin()


	// Recover function
	defer func() {
		if r := recover(); r != nil{
			tx.Rollback()
			response.Code = 500
			response.Message = "Error while processing your recharge request"
			json.NewEncoder(w).Encode(response)
			return		
		}else{
			if(sessionMismatch){
				tx.Rollback()
				response.Code = 500
				response.Message = "Session is not valid"
				json.NewEncoder(w).Encode(response)
				return
			}else{
				tx.Commit()
				lifelineInfo := model.LifelineInfo{Points: user_points, RemainingLifes: user_lifeline, UIDisplay: UIdisplayFlag}
				response.Code = 200
				response.Message = "Lifeline Info Retrieved"
				response.Data = lifelineInfo
				json.NewEncoder(w).Encode(response)
				return
			}
		}
	}()

	checkForPanic(err)


	// Decoding the request body and putting it struct
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var input_json model.RechargePointsInputJson
	err = decoder.Decode(&input_json)
	checkForPanic(err)

	// Check whether the session exists
	var userSessionIdToken string
	row := db.QueryRow(constants.CheckSessionIdToken, input_json.UserId)

	err = row.Scan(&userSessionIdToken)
	checkForPanic(err)

	if userSessionIdToken != input_json.IdToken {
		fmt.Println("IdToken is not matching")
		sessionMismatch = true
		return
	}

	fmt.Println("Getting " + input_json.UserId + " points and lifeline")

	row = db.QueryRow(constants.GetPointsAndLifelineOfUser,input_json.UserId)
	err = row.Scan(&user_points, &user_lifeline)
	checkForPanic(err)

	fmt.Println("Points", user_points)
	fmt.Println("User Lifeline",user_lifeline)


	// Check whether the point is below 1000
	if user_points > 1000 {
		fmt.Println("User is having more than 1000")
		UIdisplayFlag = false
		return
	}

	// Checking the total lifeline
	if user_lifeline <= 0 {
		fmt.Println("Users don't have anymore lifelines")
		UIdisplayFlag = false
		return
	}

	// Check the number of opened matches he haven't bidded yet
	var count int
	row = db.QueryRow(constants.GetOpenedMatchAndBiddedAmount,input_json.UserId)
	err = row.Scan(&count)
	checkForPanic(err)

	fmt.Println("How many opened matches he didn't bidded", count)
	if count != 0 {
		fmt.Println("He already bidded for the match or the match has not yet opened")
		UIdisplayFlag = false
		return
	}

	UIdisplayFlag = true
}	
