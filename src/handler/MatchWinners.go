package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func GetMatchWinners(w http.ResponseWriter, r *http.Request){
	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.MatchWinnerResponse

	defer func() {
		if r := recover(); r != nil{
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return		
		}
	}()
	fmt.Println("Fetching MatchWise Winner")

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	var match_id string 
	err :=  db.QueryRow(constants.GetLatestResultKnownMatchId).Scan(&match_id)
	checkForPanic(err)

	rows, err := db.Query(constants.MatchWinnerQuery,match_id,match_id)
	defer rows.Close()
	checkForPanic(err)

	
	var user_name string
	var profile_url string
	var points_won int
	var team1_id string
	var team2_id string
	var bid_points int

		
	var MatchWiseGainers []model.MatchWiseGainer
	for rows.Next() {
		err = rows.Scan(&user_name,&profile_url,&points_won,&team1_id,&team2_id,&bid_points)
		points_won = points_won - bid_points		
		MatchWiseGainers = append(MatchWiseGainers, model.MatchWiseGainer{Image: profile_url, Name: user_name, Points:points_won})
	}

	err = rows.Err()
	checkForPanic(err)


	Team_map := make(map[string]string)
	Team_map["t001"] = "AFG"
	Team_map["t002"] = "AUS"
	Team_map["t003"] = "BAN"
	Team_map["t004"] = "ENG"
	Team_map["t005"] = "IND"
	Team_map["t006"] = "NZ"
	Team_map["t007"] = "PAK"
	Team_map["t008"] = "SA"
	Team_map["t009"] = "SL"
	Team_map["t010"] = "WI"
	Team_map["TBC"] = "TBA"


	response.Code = 200
	response.Message = "MatchWinners results announced"
	response.Match = Team_map[team1_id] + " vs " + Team_map[team2_id]
	response.TopPlayers = MatchWiseGainers
	json.NewEncoder(w).Encode(response)
}