package handler

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/iplutilities"
	"github.com/ipl_server/src/model"
)

func UserHistory(w http.ResponseWriter, r *http.Request) {
	// Initialising response object
	var response model.HistoryResponse

	defer func() {
		if r := recover(); r != nil {
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	// Decoding the request body and putting it struct
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var input_json model.HistoryRequest
	err := decoder.Decode(&input_json)
	checkForPanic(err)

	fmt.Println("Fetching User history", input_json.Email)

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	rows, err := db.Query(constants.GetUserHistory, input_json.Email)
	defer rows.Close()
	checkForPanic(err)

	var UserHistories []model.UserHistory

	Team_map := make(map[string]string)
	Team_map["t001"] = "AFG"
	Team_map["t002"] = "AUS"
	Team_map["t003"] = "BAN"
	Team_map["t004"] = "ENG"
	Team_map["t005"] = "IND"
	Team_map["t006"] = "NZ"
	Team_map["t007"] = "PAK"
	Team_map["t008"] = "SA"
	Team_map["t009"] = "SL"
	Team_map["t010"] = "WI"
	Team_map["DRAW"] = "DRAW"
	Team_map["ABANDONED"] = "ABANDONED"

	for rows.Next() {
		var match_id string
		var bid_points int
		var team1_id string
		var team2_id string
		var team_won_id string
		var player_won_id string
		var is_result_known bool
		var points_won int
		var points_after int
		var reduced_points int
		var opted_team string
		var points_before int
		var opted_player string
		var opted_player_name sql.NullString
		var opted_player_url sql.NullString

		err := rows.Scan(&match_id, &bid_points, &team1_id, &team2_id, &team_won_id, &player_won_id, &is_result_known, &points_won, &points_after, &reduced_points, &opted_team, &points_before, &opted_player, &opted_player_name, &opted_player_url)
		checkForPanic(err)

		if points_won == 0 && reduced_points == 0 {
			reduced_points = bid_points
		}

		if points_won != 0 {
			points_won = points_won - bid_points
		}

		playerName := iplutilities.GetValue(opted_player_name)
		playerUrl := iplutilities.GetValue(opted_player_url)

		match_no, err := strconv.Atoi(match_id[1:])
		checkForPanic(err)
		UserHistories = append(UserHistories, model.UserHistory{MatchId: match_no, BidPoints: bid_points, Team1Id: Team_map[team1_id], Team2Id: Team_map[team2_id], TeamWonId: Team_map[team_won_id], PlayerWonId: player_won_id, PointsWon: points_won, PointsAfter: points_after, ReducedPoints: reduced_points, OptedTeam: Team_map[opted_team], PointsBefore: points_before, OptedPlayerId: opted_player, OptedPlayerName: playerName, OptedPlayerUrl: playerUrl, IsResultKnown: is_result_known})
	}

	err = rows.Err()
	checkForPanic(err)

	response.Code = 200
	response.Message = "Retrieved user history successfully"
	response.History = UserHistories
	json.NewEncoder(w).Encode(response)
	return

}
