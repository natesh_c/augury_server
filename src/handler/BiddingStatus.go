package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func BiddingStatus(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.CommonResponse

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	// Transaction Begins
	tx, err := db.Begin()

	// Recover function
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	checkForPanic(err)

	var request model.BiddingStatusRequestJson
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	err = decoder.Decode(&request)
	checkForPanic(err)

	if request.OpenStatus {
		fmt.Println("Input json from Opening the bidding: ", request)

		// Insert entry on Bid_history table initially
		stmt, err := tx.Prepare(constants.InsertMatchEntryOnBidHistory)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		stmt, err = tx.Prepare("UPDATE `Match` SET is_open = 'OPENED' WHERE match_id = (?)")
		defer stmt.Close()
		checkForPanic(err)

		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		stmt, err = tx.Prepare("INSERT INTO user_history (user_id, match_id, bid_points, points_before, points_won) SELECT user_id, (?), (?), points, 0 FROM  User")
		defer stmt.Close()
		checkForPanic(err)

		_, err = stmt.Exec(request.MatchId, 0)
		checkForPanic(err)

		tx.Commit()
		response.Code = 200
		response.Message = "Bidding is Opened for match " + request.MatchId
		json.NewEncoder(w).Encode(response)

	} else {

		fmt.Println("Input json from Closing the bidding: ", request)

		// // Updates the points_before for those who bidded for other matches
		_, err = db.Exec(constants.SafeUpdatesOff)

		stmt, err := tx.Prepare(constants.UpdatePointsBeforeOnBidClose)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		_, err = db.Exec(constants.SafeUpdatesOn)

		// Update user_history table for Non participants who has greater than 1000
		stmt, err = tx.Prepare(constants.UpdateUserHistoryNonParticipants)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		// Update user_history table for Non participants who has lesser or equal to 1000
		stmt, err = tx.Prepare(constants.UpdateUserHistoryNonParticipantsBelowThousand)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		// Update the User table for the Non participants
		stmt, err = tx.Prepare(constants.UpdateNonParticipantsUserPoints)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec()
		checkForPanic(err)

		stmt, err = tx.Prepare(constants.UpdateUserHistoryIsOpen)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		stmt, err = tx.Prepare(constants.UpdateMatchClosed)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.MatchId)
		checkForPanic(err)

		_, err = db.Exec(constants.SafeUpdatesOff)
		checkForPanic(err)

		stmt, err = tx.Prepare(constants.UpdateBidHistory)
		defer stmt.Close()
		checkForPanic(err)
		_, err = stmt.Exec(request.Team1Id, request.MatchId, request.Team2Id, request.MatchId, request.Team1Id, request.MatchId, request.Team2Id, request.MatchId, request.MatchId, request.MatchId)
		checkForPanic(err)

		_, err = db.Exec(constants.SafeUpdatesOn)
		checkForPanic(err)

		tx.Commit()
		response.Code = 200
		response.Message = "Bidding is Closed for match " + request.MatchId
		json.NewEncoder(w).Encode(response)
	}

}
