package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	_ "time"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func Recharge(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.CommonResponse
	var notEligible bool = false

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	// Transaction Begins
	tx, err := db.Begin()

	// Recover function
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			response.Code = 500
			response.Message = "Error while processing your recharge request"
			json.NewEncoder(w).Encode(response)
			return
		} else {
			if notEligible {
				tx.Rollback()
				response.Code = 403
				response.Message = "Currently your are not eligible for the recharge option"
				json.NewEncoder(w).Encode(response)
				return
			} else {
				tx.Commit()
				response.Code = 200
				fmt.Println("Recharge Success")
				response.Message = "Recharge Success"
				json.NewEncoder(w).Encode(response)
				return
			}
		}
	}()

	checkForPanic(err)

	// Decoding the request body and putting it struct
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var input_json model.RechargePointsInputJson
	err = decoder.Decode(&input_json)
	checkForPanic(err)

	// Check whether the session exists
	var userSessionIdToken string
	row := db.QueryRow(constants.CheckSessionIdToken, input_json.UserId)

	err = row.Scan(&userSessionIdToken)
	checkForPanic(err)

	if userSessionIdToken != input_json.IdToken {
		fmt.Println("IdToken is not matching")
		notEligible = true
		return
	}

	// TODO: Comment it when World cub begins
	// fmt.Println("Match finised: Unable to recharge")
	// notEligible = true
	// return

	fmt.Println("Recharging " + input_json.UserId + " points")

	// Get the User Details
	var leaderboard_points int
	var user_lifeline int

	row = db.QueryRow(constants.GetPointsAndLifelineOfUser, input_json.UserId)
	err = row.Scan(&leaderboard_points, &user_lifeline)
	checkForPanic(err)

	fmt.Println("Points", leaderboard_points)
	fmt.Println("User Lifeline", user_lifeline)

	// Check whether the point is below 1000
	if leaderboard_points > 1000 {
		fmt.Println("User is having more than 1000")
		notEligible = true
		return
	}

	// Checking the total lifeline
	if user_lifeline <= 0 {
		fmt.Println("Users don't have anymore lifelines")
		notEligible = true
		return
	}

	// Check the number of opened matches he haven't bidded yet
	var count int
	row = db.QueryRow(constants.GetOpenedMatchAndBiddedAmount, input_json.UserId)
	err = row.Scan(&count)
	checkForPanic(err)

	fmt.Println("How many opened matches he didn't bidded", count)
	if count != 0 {
		fmt.Println("He already bidded for the match or the match has not yet opened")
		notEligible = true
		return
	}

	// User is eligible
	notEligible = false

	// Update the User points and leaderboard points
	stmt, err := tx.Prepare(constants.UpdateLifelineToUsers)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.UserId)
	checkForPanic(err)

	// Update the points before for the opened match
	stmt, err = tx.Prepare(constants.UpdatePointsBefore)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.UserId, input_json.UserId)
	checkForPanic(err)
}
