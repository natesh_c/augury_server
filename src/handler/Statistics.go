package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
	"github.com/ipl_server/src/constants"
)

func GetStatistics(w http.ResponseWriter, r *http.Request) {
	
	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")


	fmt.Println("Fetching statistics...")
		
	var response model.StatisticsResponse
	
	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	// Recover function
	defer func() {
		if r := recover(); r != nil{
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return		
		}
	}()

	// Decoding the request body and putting it struct
	decoder := json.NewDecoder(r.Body)
	defer r.Body.Close()

	var input_json model.StatisticsInputJson
	err := decoder.Decode(&input_json)
	checkForPanic(err)

	var StatisticsArray []model.StatisticsOutputJson
	var optedTeamCategory int = 0
	var totalUsers float32 = 0
	var totalAmount float32 = 0
	
	// Getting total supporters & bid points
	rows, err := db.Query(constants.GetMatchStatistics,input_json.MatchId)
	defer rows.Close()
	checkForPanic(err)

	
	for rows.Next(){
		var opted_team string
		var supporters float32 
		var bid_points float32 
		
		err = rows.Scan(&opted_team,&supporters,&bid_points)
		checkForPanic(err)
		
		if (opted_team == ""){
			StatisticsArray = append(StatisticsArray, model.StatisticsOutputJson{Team: "Non participants", Supporters : supporters, BidAmount: supporters * 500})
			totalUsers = totalUsers + supporters
			totalAmount = totalAmount + (supporters * 500)
		} else {
			Team_map := make(map[string]string)
			Team_map["t001"] = "AFG"
			Team_map["t002"] = "AUS"
			Team_map["t003"] = "BAN"
			Team_map["t004"] = "ENG"
			Team_map["t005"] = "IND"
			Team_map["t006"] = "NZ"
			Team_map["t007"] = "PAK"
			Team_map["t008"] = "SA"
			Team_map["t009"] = "SL"
			Team_map["t010"] = "WI"
		
			totalUsers = totalUsers + supporters
			totalAmount = totalAmount + bid_points

			StatisticsArray = append(StatisticsArray, model.StatisticsOutputJson{Team: Team_map[opted_team], Supporters : supporters, BidAmount: bid_points})
			optedTeamCategory ++;
		}
	}
	

	err = rows.Err()
	checkForPanic(err)

	if(optedTeamCategory==2){
		
		for i := 0; i < len(StatisticsArray); i++ {
			StatisticsArray[i].Supporters = (StatisticsArray[i].Supporters / totalUsers ) * 100
			StatisticsArray[i].BidAmount = (StatisticsArray[i].BidAmount / totalAmount ) * 100
		}

		response.Code = 200 
		response.Message = "Statistics retrieved."
		response.Statistics = StatisticsArray
		json.NewEncoder(w).Encode(response)
	}else{
		response.Code = 500
		response.Message = "Pie chart cannot be generated for a single supported team"
		json.NewEncoder(w).Encode(response)
	}
	
}
