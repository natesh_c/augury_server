package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func AbandonedWithoutToss(w http.ResponseWriter, input_json model.UpdateResultInputJson) {

	fmt.Println("The Result is ", input_json.TeamId)

	var response model.CommonResponse

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	// Transaction Begins
	tx, err := db.Begin()

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
			response.Code = 500
			response.Message = "Problem happened while updating the result during abandoned without toss"
			json.NewEncoder(w).Encode(response)
		}
	}()

	checkForPanic(err)

	_, err = tx.Query(constants.SafeUpdatesOff)
	checkForPanic(err)

	// Updating the points in the User Table - bidded
	stmt, err := tx.Prepare(constants.UpdatePointsOnDraw)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId)
	checkForPanic(err)

	// Updating the points in the User Table - Non participants
	stmt, err = tx.Prepare(constants.UpdatePointsOnAbandonedNonParticipants)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId)
	checkForPanic(err)

	//Updating the afterpoints in the user_history
	stmt, err = tx.Prepare(constants.UpdateAfterPointsOnAbandoned)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.MatchId)
	checkForPanic(err)

	//Updating the teamWon in BidHistory
	stmt, err = tx.Prepare(constants.UpdateTeamWonOnDraw)
	defer stmt.Close()
	checkForPanic(err)

	_, err = stmt.Exec(input_json.TeamId, input_json.MatchId)
	checkForPanic(err)

	_, err = tx.Query(constants.SafeUpdatesOff)
	checkForPanic(err)

	tx.Commit()
	response.Code = 200
	response.Message = "The Match is " + input_json.TeamId
	json.NewEncoder(w).Encode(response)
}
