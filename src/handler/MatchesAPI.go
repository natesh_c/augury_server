package handler

import (
	"database/sql"
	"encoding/json"
	_ "fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/iplutilities"
	"github.com/ipl_server/src/model"
)

func GetCurrentAndFutureMatches(w http.ResponseWriter, r *http.Request) {

	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.MatchesResponse

	defer func() {
		if r := recover(); r != nil {
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return
		}
	}()

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	rows, err := db.Query(constants.AllMatchesQuery)
	defer rows.Close()
	checkForPanic(err)

	var Matches []model.MatchSchedule

	VenueMap := make(map[string]string)
	VenueMap["v001"] = "Kennington Oval, London"
	VenueMap["v002"] = "Trent Bridge, Nottingham"
	VenueMap["v003"] = "County Ground, Bristol"
	VenueMap["v004"] = "The Rose Bowl, Southampton"
	VenueMap["v005"] = "The Cooper Associates County Ground, Taunton"
	VenueMap["v006"] = "Emirates Old Trafford, Manchester"
	VenueMap["v007"] = "Edgbaston, Birmingham"
	VenueMap["v008"] = "Headingley, Leeds"
	VenueMap["v009"] = "Lord's, London"
	VenueMap["v010"] = "Riverside Ground, Chester-le-Street"
	VenueMap["v011"] = "Sophia Gardens, Cardiff"

	Team_map := make(map[string]string)
	Team_map["t001"] = "Afghanistan"
	Team_map["t002"] = "Australia"
	Team_map["t003"] = "Bangladesh"
	Team_map["t004"] = "England"
	Team_map["t005"] = "India"
	Team_map["t006"] = "New Zealand"
	Team_map["t007"] = "Pakistan"
	Team_map["t008"] = "South Africa"
	Team_map["t009"] = "Sri Lanka"
	Team_map["t010"] = "Windies"
	Team_map["TBC"] = "TBA"

	for rows.Next() {
		var match_id string
		var team1_id string
		var team2_id string
		var start_date string
		var venue_id string
		var participants string
		var is_open string
		var team_won_id sql.NullString
		var is_result_known sql.NullBool
		var mom_player sql.NullString
		var mom_player_url sql.NullString

		err := rows.Scan(&match_id, &team1_id, &team2_id, &start_date, &venue_id, &is_open, &team_won_id, &is_result_known, &mom_player, &mom_player_url)
		checkForPanic(err)

		// Get the match no from the match id
		match_no, err := strconv.Atoi(match_id[1:])
		checkForPanic(err)

		// Getting the venue name from the venue_id
		venue_id = VenueMap[venue_id]
		participants = Team_map[team1_id] + " vs " + Team_map[team2_id]

		//convert to milliseconds
		s := strings.Split(start_date, "+")

		t1, _ := time.Parse(
			time.RFC3339,
			s[0]+":00"+"+"+s[1])

		startDateInMilliSeconds := (t1.UnixNano() / int64(time.Millisecond))

		// Get proper STRING results without Null
		won_id := iplutilities.GetValue(team_won_id)
		playerName := iplutilities.GetValue(mom_player)
		playerUrl := iplutilities.GetValue(mom_player_url)
		isResultKnown := iplutilities.GetBoolValue(is_result_known)

		Matches = append(Matches, model.MatchSchedule{MatchNo: match_no, Participants: participants, DateTime: start_date, Venue: venue_id, Team1Id: team1_id, Team2Id: team2_id, StartDateInMilliSeconds: startDateInMilliSeconds, IsOpen: is_open, TeamWonId: won_id, IsResultKnown: isResultKnown, MomPlayerName: playerName, MomPlayerUrl: playerUrl})
	}

	err = rows.Err()
	checkForPanic(err)

	response.Code = 200
	response.Matches = Matches
	response.Message = "All the current and future matches are retreived"
	json.NewEncoder(w).Encode(response)
}
