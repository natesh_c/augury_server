package handler

import(
	"fmt"
	"net/http"
	"encoding/json"
	"github.com/ipl_server/src/model"
	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
)

func AdminAuth(w http.ResponseWriter, r *http.Request){
		w.Header().Set("Access-Control-Allow-Origin", "*")

		var response model.CommonResponse

		defer func() {
			if r := recover(); r != nil{
				response.Code = 500
				response.Message = "Error while processing your request"
				json.NewEncoder(w).Encode(response)
				return		
			}
		}()


		decoder := json.NewDecoder(r.Body)
		var input_json model.AdminLoginRequest
		err := decoder.Decode(&input_json)
		checkForPanic(err)




		// Connection to database
		db := dbutilities.DbConnect()
		defer db.Close()

		rows, err := db.Query(constants.AdminDetails)
		defer rows.Close()
		checkForPanic(err)


		for rows.Next() {
			var name string;
			var password string;

			err = rows.Scan(&name,&password)
			checkForPanic(err)
			if input_json.Email == name && input_json.Password == password {
				fmt.Println("Admin login successful")
				response.Code = 200
				response.Message = "Login Authenticated"
				json.NewEncoder(w).Encode(response)
				return
			}
		}

		response.Code = 400
		response.Message = "Wrong Email or Password"
		json.NewEncoder(w).Encode(response)
}