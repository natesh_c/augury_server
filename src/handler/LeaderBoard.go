package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"github.com/ipl_server/src/constants"
	"github.com/ipl_server/src/dbutilities"
	"github.com/ipl_server/src/model"
)

func LeaderBoard(w http.ResponseWriter, r *http.Request){
	// Enable CORS Support
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var response model.LeaderboardResponse

	defer func() {
		if r := recover(); r != nil{
			response.Code = 500
			response.Message = "Error while processing your request"
			json.NewEncoder(w).Encode(response)
			return		
		}
	}()

	fmt.Println("Fetching Leaderboard")

	// Connection to database
	db := dbutilities.DbConnect()
	defer db.Close()

	rows, err := db.Query(constants.LeaderBoardQuery)
	checkForPanic(err)
	defer rows.Close()

	var rank int = 0
	var previous_points int = 0
	var UsersRank []model.UserRank

	for rows.Next() {
		var name string
		var points int
		var profile_url string

		err = rows.Scan(&name,&points,&profile_url)
		checkForPanic(err)

		if (points != previous_points){
			rank += 1
		}

		UsersRank = append(UsersRank, model.UserRank{Rank: rank, Picture: profile_url, Name: name, Points: points})
		previous_points = points
	}

		err = rows.Err()
		checkForPanic(err)

	if len(UsersRank) > 0 {
		response.TopPoints = UsersRank
		} else {
			response.TopPoints = nil
		}
	

	response.Code = 200
	response.Message = "Leaderboard response successfully returned"
	json.NewEncoder(w).Encode(response)
}